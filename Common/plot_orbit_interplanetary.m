function [] = plot_orbit_interplanetary (time_departure, time_arrival, planet_1, planet_2)

time_departure_s = 24*3600*date2mjd2000(time_departure);
time_arrival_s = 24*3600*date2mjd2000(time_arrival);
time_of_flight = time_arrival_s - time_departure_s;

[orbit1,ksun] = uplanet(time_departure_s,planet_1);
[orbit2,ksun] = uplanet(time_arrival_s,planet_2);

[r1,v1]=kp2rv_orbit(orbit1,mu);
[r2,v2]=kp2rv_orbit(orbit2,mu);
[A,P,E,ERROR,VI,VF,TPAR,THETA] = lambertMR(r1, r2, time_of_flight, ksun, 0, 0, 0, 2);


options=odeset('RelTol',10^(-13),'AbsTol',1*10^(-14));
fun=@(t,u) [u(4); u(5); u(6); -(ksun.*u(1))./((u(1).^2+u(2).^2+u(3).^2).^(3/2)); -(ksun.*u(2))./((u(1).^2+u(2).^2+u(3).^2).^(3/2)); -(ksun.*u(3))./((u(1).^2+u(2).^2+u(3).^2).^(3/2))];
[tode_1,X_1]=ode113(fun,linspace(0,2*pi*sqrt((orbit1(1).^3)/(ksun)),1000),[r1(1); r1(2); r1(3); v1(1); v1(2); v1(3)],options);
[tode_2,X_2]=ode113(fun,linspace(0,2*pi*sqrt((orbit2(1).^3)/(ksun)),1000),[r2(1); r2(2); r2(3); v2(1); v2(2); v2(3)],options);
[tode_T,X_T]=ode113(fun,linspace(0,time_of_flight,1000),[r1(1); r1(2); r1(3); VI(1); VI(2); VI(3)],options);

figure
plot3(r1(1),r1(2),r1(3))
hold on
plot3(r2(1),r2(2),r3(3))
plot3(X_T(1,:),X_T(2,:),X_T(3,:))
lengend('initial point','final point','transfer orbit')

