function [] = GraphTitle(n)
% Function used to give the title to certain graph automatically thanks to
% n.
%
% INPUT: 
% n          number that indicates which parameters we are referring
% OUTPUT:
% puts the title to the graph
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

switch n
        case 1
            title('Semimajor axis')
        case 2
            title('Eccentricity')
        case 3 
            title('Inclination')
        case 4
            title('Rigth ascension of the ascending node')
        case 5
            title('Argument of perigee')
        case 6
            title('True anomaly')
    end
return