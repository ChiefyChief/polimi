function k = KeplerianPerturbation(t, kep, mu, planet_radius, J2, drag_parameters)
% Functions used to save the raw data in a more handle way
%
% INPUT: 
% t               time vector
% kep             keplerian parameters
% mu              gravitational constant
% planet_radius   radius of the panet considered
% J2              perturbation 
% drag_parameters vector containing the parameters linked with the J2
%                 effects
%
% OUTPUT:
% KeplerianPerturation  struct containing all the important parameters of
%                       the perturbed parameters
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

%% Setup
h = kep(1);
e = kep(2);
i = kep(3);
omega = kep(4);
omeghino = kep(5);
theta = kep(6);

a = ( h^2 / mu ) * ( 1 / (1 - e^2) );

keplerian_elements.semimajor_axis = a;
keplerian_elements.eccentricity = e;
keplerian_elements.inclination = i;
keplerian_elements.RAAN = kep(4);
keplerian_elements.argument_of_perigee = omeghino;
keplerian_elements.true_anomaly = theta;

theta_star = omeghino + theta;

a_m_ratio = drag_parameters.a_m_ratio;
cd = drag_parameters.cd;
omega_earth = drag_parameters.omega_earth;

%% Convert to Cartesian to calculate drag perturbation

[cartesian.r, cartesian.v] = Keplerian2CartesianOrbit(keplerian_elements, mu);

%% Calculate Drag Perturbation

% Calculate which atmospheric layer the craft is in
altitude = norm(cartesian.r) - planet_radius; 

if isnan(altitude)
    disp("NAN");
end
[density_parameters] = DensityModel(altitude); 

reference_altitude = density_parameters.h_0; 
reference_density = density_parameters.rho_0; 
scale_height = density_parameters.H; 

atmospheric_drag_multiplier = -(1/2) * a_m_ratio * cd * reference_density * exp(-( (altitude - reference_altitude) / scale_height) );

v_relative = cartesian.v - cross(omega_earth, cartesian.r);

drag_cartesian = atmospheric_drag_multiplier .* (norm(v_relative)^2) .* v_relative ./ norm(v_relative);

rotation_matrix = ...
[
-sin(omega) * cos(i) * sin(theta_star) + cos(omega) * cos(theta_star), cos(omega) * cos(i) * sin(theta_star) + sin(omega) * cos(theta_star), sin(i) * sin(theta_star);
-sin(omega) * cos(i) * cos(theta_star) - cos(omega) * sin(theta_star), cos(omega) * cos(i) * cos(theta_star) - sin(omega) * sin(theta_star), sin(i) * cos(theta_star);
sin(omega) * sin(i), -cos(theta) * sin(i), cos(i)
];

drag_nth = rotation_matrix * drag_cartesian;
%%

n = sqrt(mu/a^3);
b = a*sqrt(1-e^2);
h = n*a*b;
p = b^2/a;
r = p/(1+e*cos(theta));

% Pertubing acceleration due to J2 only 
j2_premultiplier = - (mu / r^2) * (3/2) * J2 * ((planet_radius / r)^2);

j2_n = j2_premultiplier * ( 1 - 3 * sin(i)^2 * sin(theta_star)^2 );
j2_t = j2_premultiplier *  sin(i)^2 * sin(2 * theta_star);
j2_h = j2_premultiplier * ( sin(2 * i) * sin(theta_star) );

p_n = j2_n + drag_nth(1);
p_t = j2_t + drag_nth(2);
p_h = j2_h + drag_nth(3);

d_h = r * p_t;

d_e = (h / mu) * sin(theta) * p_n + (1 / (mu * h)) * ((h^2 + mu * r) * cos(theta) + mu * e * r) * p_t;

d_i = (r / h) * cos(theta_star) * p_h;

d_omega = (r / (h * sin(i))) * sin(theta_star) * p_h;

d_omeghino = - (1 / (e * h)) * ((h^2 / mu) * cos(theta) * p_n - (r + (h^2 / mu)) * sin(theta) * p_t) - (r * sin(theta_star) / (h * tan(i))) * p_h;

d_theta = (h / r^2) + (1 / (e * h)) * ((h^2 / mu) * cos(theta) * p_n - (r + (h^2 / mu)) * sin(theta) * p_t);


k = [
    
d_h;

d_e;

d_i;

d_omega;

d_omeghino;

d_theta;

];
                    
end    