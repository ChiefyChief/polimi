function [orbit] = RepeatingGroundTrackJ2 (orbit)
% Find the semimajor axis of a repeating ground track with the secular J2 perturbation
% 
% PROTOTYPE
% [orbit] = RepeatingGroundTrackJ2 (orbit)
%
% INPUTS
%   orbit       struct containing the orbital parameters and the following
%               parameters:
%                           omega_E = angular velocity of Earth [rad/T]
%                           J2 = oblateness value of Earth [-]
%                           repeating GT ration [-]
%
% OUTPUTS
%   orbit       struct containing the orbital parameters of the modified orbit
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
%
%Function needed:
%       astroConstants

Re = astroConstants(23);
omegaE = norm(orbit.omega_E);
J2 = orbit.J2;
k = 1/orbit.repeating_GT_ratio;
mu = orbit.gravitational_constant;
e = orbit.eccentricity;
i = orbit.inclination;

fun=@(a) (k-(omegaE+1.5*sqrt(mu)*J2*Re.^2/((1-e^2)^2*a^3.5)*cos(i))/(sqrt(mu/a.^3)+1.5*sqrt(mu)*J2*Re.^2/((1-e^2)^2*a^3.5)*(2.5*(sin(i).^2)-2)-1.5*sqrt(mu)*J2*Re.^2/((1-e^2)^2*a^3.5)*(1-1.5*sin(i).^2)));
options = optimset('TolFun',1e-32,'TolFun',1e-32);

% computation of the semimajor axis
orbit.Modified_orbit_J2.semimajor_axis = fzero(fun,orbit.Modified_orbit.semimajor_axis,options);

orbit.Modified_orbit_J2.eccentricity = orbit.eccentricity;
orbit.Modified_orbit_J2.inclination = orbit.inclination;
orbit.Modified_orbit_J2.RAAN = orbit.RAAN;
orbit.Modified_orbit_J2.argument_of_perigee = orbit.argument_of_perigee;
orbit.Modified_orbit_J2.true_anomaly = orbit.true_anomaly;
orbit.Modified_orbit_J2.period = 2*pi*sqrt(orbit.Modified_orbit_J2.semimajor_axis.^3/mu);
orbit.Modified_orbit_J2.gravitational_constant = mu;
