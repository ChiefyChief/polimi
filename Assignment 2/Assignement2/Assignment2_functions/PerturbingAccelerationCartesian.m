function [cartesian_perturbation] = PerturbingAccelerationCartesian(orbit, n)

%INPUT
%orbit = struct with the orbital parameters(look at the Main)
%N = number of orbits

%OUTPUT
%CartesianPertubation = strcut with field
%                       y = results of the ode integrator

%DESCRIPTION 
%The function solve the differential equation thanks to the odesolver,
%using ode113. The ode is done step by step in order to identify the single
%layer of atmosphere where the s/c is during the orbit due to perturbating
%actions

% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

drag_parameters.a_m_ratio = orbit.A_m_ratio;
drag_parameters.cd = orbit.drag_coefficient;
drag_parameters.omega_earth = orbit.omega_E;
mu_earth = orbit.gravitational_constant;
earth_radius = orbit.Earth_radius;
J2 = orbit.J2;

%T = orbit.period;
t = linspace (0,n*86400,n*500);

cartesian_perturbation.y = zeros(6,length(t));

% Initial position and velocity
[r0,v0] = Keplerian2CartesianOrbit(orbit,mu_earth);

y0 = [r0;v0];
cartesian_perturbation.y(:,1) = y0;

options=odeset('RelTol',1e-15,'AbsTol',1e-16);

[t,y] = ode113(@(t,y) CartesianPerturbation(y, mu_earth, earth_radius, J2, drag_parameters),[t(1) t(end)], y0, options);
cartesian_perturbation.y = y;
cartesian_perturbation.counter_period = 1;
cartesian_perturbation.A = [];

for i = 1:length(y(:,1))
    [cartesian_perturbation.orbital_parameters(i)] = Cartesian2KeplerianOrbit(y(i,1:3), y(i, 4:6), mu_earth);
end

for i = 2:length(y(:,1))
    if abs(cartesian_perturbation.orbital_parameters(i).true_anomaly-cartesian_perturbation.orbital_parameters(i-1).true_anomaly) > 1.5*pi
        cartesian_perturbation.A = [cartesian_perturbation.A;i];
        cartesian_perturbation.counter_period = cartesian_perturbation.counter_period + 1;
    end
end

cartesian_perturbation.time = t;

