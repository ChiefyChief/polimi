function [keplerian_perturbation] = PerturbingAccelerationKeplerian(orbit, n)

%INPUT
%orbit = struct with all the orbital parameters (look at the Main)

%OUTPUT 
%KeplerianPerturbation = ode solver result 

%DESCRITION 
%The function does the integration of the equation of motion considering
%keplerian parameters.

%Even the pertubations are in keplerian coordinates 
%Keplerian Vector [a e i RAAN omeghino theta M]. Thi is the vector used as
%reference in de ode solver in ordere to integrate the following equations:

% Da = ((2*a^2*v)/mu)*a_t;
% De = (1/v)*((2*(ecc+cos(theta))*a_t)-((r/a)*sin(theta)*a_n));
% Di = ((r*cos(theta_s))/h)*a_h
% DRAAN = ((r*sin(theta_s))/(h*sin(i)))*a_h
% DOmeghino = ((1/(ecc*v))*((2*sin(theta)*a_t)+(((2*ecc)+((r/a)*cos(theta)))*a_n)))-(((r*sin(theta_s)*cos(i))/(h*sin(i)))*a_h)
% Dtheta = (h/r^2)-((1/(ecc*v))*(2*sin(theta)*a_t+(2*ecc+(r/a)*cos(theta))*a_n));
% DM = n-((b/(e*a*v))*((2*(1+((ecc^2*r)/p))*sin(theta)*a_t)+((r/a)*cos(theta)*a_n)))

%All of them are conteined in the function KeplerianPerturbationFunc where
%the dependence on the keplerian parameters is highlighted

%Other parameters inside the equation above: 
% n = sqrt(mu/a^3)
% b = 	
% h = (sqrt(mu/a^3))*a*a*sqrt(1-e^2))
% p = (a*sqrt(1-e^2))^2/a
% r = ((a*sqrt(1-e^2))^2/a)/(1+e*cos(theta))
% v = sqrt(((2*mu)/(((a*sqrt(1-e^2))^2/a)/(1+e*cos(theta))))-(mu/a))
% theta_s = omeghino+theta
%
%The expression are reported here for clearance because their expression
%are not readable anymore
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

mu_earth = orbit.gravitational_constant;

a = orbit.semimajor_axis;
mean = sqrt(mu_earth/a^3);
b = a * sqrt(1 - orbit.eccentricity^2);

keplerian_initial(1) = mean * a * b;

keplerian_initial(2) = orbit.eccentricity;
keplerian_initial(3) = orbit.inclination;
keplerian_initial(4) = orbit.RAAN;
keplerian_initial(5) = orbit.argument_of_perigee;
keplerian_initial(6) = orbit.true_anomaly;
%keplerian_initial(7) = -keplerian_initial(2)*sin(keplerian_initial(6));

drag_parameters.a_m_ratio = orbit.A_m_ratio;
drag_parameters.cd = orbit.drag_coefficient;
drag_parameters.omega_earth = orbit.omega_E;
earth_radius = orbit.Earth_radius;
J2 = orbit.J2;

t = linspace(0, n*86400, n*500);

keplerian_perturbation.y = zeros (6,length(t));
keplerian_perturbation.y(:,1) = keplerian_initial;

options=odeset('RelTol',1e-15,'AbsTol',1e-16);

%result
[t,y] = ode113(@(t,y) KeplerianPerturbation(t, y, mu_earth, earth_radius, J2, drag_parameters), [t(1), t(end)], keplerian_initial, options);
    
for i = 1:length(y(:,1))
    
    h = y(i,1);
    e = y(i,2);
    
    semimajor_axis = ( h^2 / mu_earth ) * ( 1 / (1 - e^2) );
    
    keplerian_perturbation.orbital_parameters(i).semimajor_axis = semimajor_axis;
    keplerian_perturbation.orbital_parameters(i).eccentricity = e;
    keplerian_perturbation.orbital_parameters(i).inclination = y(i,3); 
    keplerian_perturbation.orbital_parameters(i).RAAN = wrapTo2Pi(y(i,4));
    keplerian_perturbation.orbital_parameters(i).argument_of_perigee = wrapTo2Pi(y(i,5));
    keplerian_perturbation.orbital_parameters(i).true_anomaly = wrapTo2Pi(y(i,6));
 %   keplerian_perturbation.orbital_parameters(i).mean_anomaly = y(i,7);
end
    
keplerian_perturbation.time = t;

end