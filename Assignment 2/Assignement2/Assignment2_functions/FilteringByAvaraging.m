function [signal] = FilteringByAvaraging(A,signal_frequency,k,time)
% Does the graph in frequency of the input signal
%
% INPUT:
% A                      signal
% signal_frequency       frequency of the signal
% k                      number over which the avarage is done
% time                   vector containing all the time step considered 
%
% OUTPUT:
% signal                 Filtered signal
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

figure;
for n = 1 : 6
    subplot(3,2,n)
    signal_moving_avg(:,n) = movmean(A(:,n), k);
    plot(time, signal_moving_avg(:,n));
    GraphTitle(n);
    if n == 1
       ylabel('[km]')
    else
       ylabel('deg(�)') 
    end
    xlabel('time(day)')
end

figure;
for n = 1 :6
    subplot(3,2,n)
    [signal_low_frequency(:,n),~] = SpectralAnalysis(time, signal_moving_avg(:,n), n);

end

% High frequencies
signal_high_frequency = signal_frequency - signal_low_frequency;
signal_longer_period = ifft(ifftshift(signal_high_frequency,1))+signal_moving_avg(1,:);

figure;
for n = 1 : 6
    subplot(3,2,n)
    plot(time, signal_longer_period(:,n))
    GraphTitle(n);
    xlabel('time(s)')
    ylabel('deg(�)')
end

signal.signal_avaraged = signal_moving_avg;
signal.signal_low_frequency = signal_low_frequency;
signal.signal_high_frequency = signal_high_frequency;
signal.signal_longer_period = signal_longer_period;

return