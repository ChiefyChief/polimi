%%
%{
    Planetary Explorer Mission Analysis
    
    Group 10

    Authors:
    - Bonariol Teodoro          941028
    - Cantoni Alexia            945126
    - Capelli Anita             939522
    - Parisi Adrian Burton      940789
%}

clear variables;
clc;
close all;


% Add paths to third party common functions
addpath('../Common');
addpath('../Common/Group');

%% Main 
%% Nominal orbit
% PlanetaryExplorerData: structure with field
                        % gravitational constant {Km^3/s^2]
                        % semimajor_axis [Km]
                        % eccentricity [-]
                        % inclination [rad]
                        % perigee_altitude [Km]
                        % RAAN [rad] 
                        % argument_of perigee [rad]
                        % true_anomaly [rad] 
                        % T = period [s]
                        % drag_coefficient [-]
                        % A_m_ratio [m^2/Kg]
                        % repeating_GT_ratio [-]
                        % omega_E [rad/s]
                        % J2 [-]
                        % Earth_radius = radius of the Earth [Km];
                        
PlanetaryExplorerData.gravitational_constant = astroConstants(13);
PlanetaryExplorerData.semimajor_axis = 2.5697e4;
PlanetaryExplorerData.eccentricity = 0.7242;
PlanetaryExplorerData.inclination = deg2rad(28.5389);
PlanetaryExplorerData.perigee_altitude = 716.223; 
PlanetaryExplorerData.RAAN = deg2rad(100); 
PlanetaryExplorerData.argument_of_perigee = deg2rad(30); 
PlanetaryExplorerData.true_anomaly = deg2rad(45);
PlanetaryExplorerData.period = 2*pi*sqrt(PlanetaryExplorerData.semimajor_axis.^3/PlanetaryExplorerData.gravitational_constant);
PlanetaryExplorerData.drag_coefficient = 2.2;
PlanetaryExplorerData.A_m_ratio = 0.06;
PlanetaryExplorerData.repeating_GT_ratio = 11/5;
PlanetaryExplorerData.omega_E = 2*pi/(23*3600+56*60)*[0;0;1];
PlanetaryExplorerData.J2 = 0.00108263;
PlanetaryExplorerData.Earth_radius = astroConstants(23); 

%% Ground tracks
%% Ground track with and without the J2 effect
width = 1200;
height = 600;
% 1 orbit ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData,1,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,1,'J2','xm','sc');
legend('GT without J2','Initial position','Final position','GT with J2','Initial position','Final position','Location','SouthWest')


% 1 day ground track
% number of orbits in one day
PlanetaryExplorerData.Earth_period = 23*3600+56*60;
PlanetaryExplorerData.Number_of_orbits_in_one_day = PlanetaryExplorerData.Earth_period/PlanetaryExplorerData.period;

figure
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
set(gcf,'position',[10, 10, width, height]);
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData,PlanetaryExplorerData.Number_of_orbits_in_one_day,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,PlanetaryExplorerData.Number_of_orbits_in_one_day,'J2','xm','sc');
legend('GT without J2','Initial position','Final position','GT with J2','Initial position','Final position','Location','SouthWest')

% 10 day ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData,10*PlanetaryExplorerData.Number_of_orbits_in_one_day,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,10*PlanetaryExplorerData.Number_of_orbits_in_one_day,'J2','xm','sc');
legend('GT without J2','Initial position','Final position','GT with J2','Initial position','Final position','Location','SouthWest')

%% Repeating ground track
%% Repeating ground track without J2
% computation of the orbital period and semimajor axis for repeating ground track
PlanetaryExplorerData.Modified_orbit.period = PlanetaryExplorerData.Earth_period/PlanetaryExplorerData.repeating_GT_ratio;
PlanetaryExplorerData.Modified_orbit.semimajor_axis =(PlanetaryExplorerData.gravitational_constant.*(PlanetaryExplorerData.Modified_orbit.period./(2*pi))^2)^(1/3);
% all the other orbital parameters remain the same
PlanetaryExplorerData.Modified_orbit.eccentricity = PlanetaryExplorerData.eccentricity;
PlanetaryExplorerData.Modified_orbit.inclination = PlanetaryExplorerData.inclination;
PlanetaryExplorerData.Modified_orbit.RAAN = PlanetaryExplorerData.RAAN;
PlanetaryExplorerData.Modified_orbit.argument_of_perigee = PlanetaryExplorerData.argument_of_perigee;
PlanetaryExplorerData.Modified_orbit.true_anomaly = PlanetaryExplorerData.true_anomaly;
PlanetaryExplorerData.Modified_orbit.gravitational_constant = PlanetaryExplorerData.gravitational_constant;

% 1 orbit ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,1,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,1,'none','xm','sc');
legend('Repeating GT','Initial position','Final position','Original GT','Initial position','Final position','Location','SouthWest')

% 1 day ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,PlanetaryExplorerData.repeating_GT_ratio,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,PlanetaryExplorerData.Number_of_orbits_in_one_day,'none','xm','sc');
legend('Repeating GT','Initial position','Final position','Original GT','Initial position','Final position','Location','SouthWest')

% 10 orbits ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,10,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData,10,'none','xm','sc');
legend('Repeating GT','Initial position','Final position','Original GT','Initial position','Final position','Location','SouthWest')

%% Repeating ground track with J2
% computation of the orbital parameters for a repeating ground track
% taking in account the secular J2 effect
[PlanetaryExplorerData] = RepeatingGroundTrackJ2 (PlanetaryExplorerData);

% 1 orbit ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,1,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit_J2,1,'J2','xm','sc');
legend('Repeating','Initial position','Final position','Repeating with J2','Initial position','Final position','Location','SouthWest')

% 1 day ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,PlanetaryExplorerData.repeating_GT_ratio,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit_J2,PlanetaryExplorerData.repeating_GT_ratio,'J2','xm','sc');
legend('Repeating','Initial position','Final position','Repeating with J2','Initial position','Final position','Location','SouthWest')

% 10 orbits ground track
figure
set(gcf,'position',[10, 10, width, height]);
xlabel('Longitude [Deg]');
ylabel('Latitude [Deg]');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
PlotEarthTexture
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit,10,'none','og','dw');
[~] = GroundTrack(PlanetaryExplorerData.Modified_orbit_J2,10,'J2','xm','sc');
legend('Repeating','Initial position','Final position','Repeating with J2','Initial position','Final position','Location','SouthWest')

%% Introduction of assigned perturbations and propagation of the orbit

n = 1 * 365; % Number of days to iterate over

tic
[cartesian_perturbation] = PerturbingAccelerationCartesian(PlanetaryExplorerData, n);
toc
tic
[keplerian_perturbation] = PerturbingAccelerationKeplerian(PlanetaryExplorerData, n);
toc

%% Plot of the history of the keplerian elements
time_cartesian = cartesian_perturbation.time ./ 86400; %need mjd2000
time_keplerian = keplerian_perturbation.time ./ 86400;

[A_cart] = ParametersMatrix (cartesian_perturbation.orbital_parameters);
[A_kep] = ParametersMatrix (keplerian_perturbation.orbital_parameters);

a_c  = A_cart(:,1);
a_k  = A_kep(:,1);


figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
plot (time_cartesian, a_c,'x')
hold on 
plot (time_keplerian, a_k,'+')
xlabel ('Time [days]')
ylabel ('Semimajor Axis [km]')
grid on;
legend ('Cartesian','Keplerian');
xlim([0 365]);

e_c = A_cart(:,2);
e_k = A_kep(:,2);

figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
plot (time_cartesian, e_c,'x');
hold on 
plot (time_keplerian, e_k,'+')
xlabel ('Time [days]');
ylabel ('Eccentricity [ ]');
grid on;
legend ('Cartesian','Keplerian');
xlim([0 365]);

i_c_deg = rad2deg(A_cart(:,3));
i_k_deg = rad2deg(A_kep(:,3));
    
figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
plot (time_cartesian, i_c_deg,'x')
hold on 
plot (time_keplerian, i_k_deg,'+')
xlabel ('Time [days]')
ylabel ('Inclination [deg]')
legend ('Cartesian','Keplerian');
grid on;
xlim([0 365]);
RAAN_c_deg = rad2deg(A_cart(:,4));
RAAN_k_deg = rad2deg(A_kep(:,4));

figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
plot (time_cartesian, RAAN_c_deg,'x')
hold on 
plot (time_keplerian, RAAN_k_deg,'+')
xlabel ('Time [days]')
ylabel ('$\Omega$ [deg]')
legend ('Cartesian','Keplerian');
grid on;
xlim([0 365]);
omeghino_c_deg = rad2deg(A_cart(:,5));
omeghino_k_deg = rad2deg(A_kep(:,5));

figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
plot (time_cartesian, omeghino_c_deg,'x');
hold on 
plot (time_keplerian, omeghino_k_deg,'+')
xlabel ('Time [days]')
ylabel ('$\omega$ [deg]')
legend ('Cartesian','Keplerian');
grid on;
xlim([0 365]);
theta_c_deg = rad2deg(A_cart(:,6));
theta_k_deg = rad2deg(A_kep(:,6)); 

figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex'); 
plot (time_cartesian, theta_c_deg,'x');
hold on 
plot (time_keplerian, theta_k_deg,'+')
xlabel ('Time [days]')
ylabel ('$\theta$ [deg]')
legend ('Cartesian','Keplerian');
grid on;
xlim([0 365]);

%% Error calculation
error.semi = abs(a_c(end) - a_k(end));
error.eccentricity = abs(e_c(end) - e_k(end));
error.inclination = abs(i_c_deg(end) - i_k_deg(end));
error.RAAN = abs(RAAN_c_deg(end) - RAAN_k_deg(end));
error.omeghino = abs(omeghino_c_deg(end) - omeghino_k_deg(end));
error.theta = abs(theta_c_deg(end) - theta_k_deg(end));

%% Computional Cost 

for i = 1 : 40
    n = i * 50;
    tic
    [cartesian_perturbation] = PerturbingAccelerationCartesian(PlanetaryExplorerData, n);
    t.cart(i) = toc;
    
    tic
    [keplerian_perturbation] = PerturbingAccelerationKeplerian(PlanetaryExplorerData, n);
    t.kep(i) = toc;
    
    display("Computational Cost Iteration: " + i);
end

%% Computational Cost Graphs

computational_cost_axis = linspace(50, 40 * 50, 40);

figure;
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex'); 
plot(computational_cost_axis,t.cart,'o--');
hold on;
plot(computational_cost_axis,t.kep,'o--');
hold off;
xlabel('$n_{days}$ [ ]');
ylabel('Computational Time [s]');
grid minor;

%% Representation of the evolution of the orbit
PlotOrbitEvolution(cartesian_perturbation.y(:,1:3)',cartesian_perturbation.counter_period,cartesian_perturbation.A)

%% Representation of the evolution of the orbit after a specified number of revolutions 
% Open figure
set(0,'DefaultTextFontname','CMU Serif');
set(0,'DefaultAxesFontName','CMU Serif');
set(0,'defaulttextinterpreter','latex');
figure;


% plot of Earth
[X,Y,Z] = ellipsoid(0,0,0,6378,6378,6357);
s = surf(X,Y,-Z,'FaceColor','None','EdgeColor','None');
image = imread('Earth','jpg');
set(s,'FaceColor','texturemap','CData',image);
    
% plot setting
axis equal
grid on
hold on
xlim([-10000,45000])
ylim([-50000,50000])

view([0 -1 0.25])
PlotOrbitEvolution(cartesian_perturbation.y(:,1:3)',cartesian_perturbation.counter_period,cartesian_perturbation.A,50);
hold on;

xlabel('x [km]');
ylabel('y [km]');
zlabel('z [km]');
grid minor;
for i = 2 : 40
num = i * 15;
PlotOrbitEvolution(cartesian_perturbation.y(:,1:3)',cartesian_perturbation.counter_period,cartesian_perturbation.A,num);
end
%% Spectral analysis
% Cartesian
A = ParametersMatrix(cartesian_perturbation.orbital_parameters);
A(:,3:6) = A(:,3:6).*(180/pi);

% Keplerian
B = ParametersMatrix(keplerian_perturbation.orbital_parameters);
B(:,3:6) = B(:,3:6).*(180/pi);
day = 3600*24;

cut_off = 1 / PlanetaryExplorerData.period;


for n = 1 : 6
    i = 1;
    
    figure
    
    while i < 3
        subplot(1,2,i)
        
        if i == 1
             [cartesian_signal_frequency(:,n),frequency_cartesian] = SpectralAnalysis(cartesian_perturbation.time./cut_off,A(:,n),n);
             title('Cartesian');
        else
             [keplerian_signal_frequency(:,n),frequency_keplerian] = SpectralAnalysis(keplerian_perturbation.time./cut_off,B(:,n),n);
             title('Keplerian');
        end
        i = i + 1;
    end
end

%% Filtering of high frequencies

k_orbit_period = 2*pi*sqrt(PlanetaryExplorerData.semimajor_axis^3/PlanetaryExplorerData.gravitational_constant);

% FIRST AVERAGING
%Cartesian
[cartesian_signal_avaraged_1] = FilteringByAvaraging(A,cartesian_signal_frequency,k_orbit_period,cartesian_perturbation.time./day);

%Keplerian
[keplerian_signal_avaraged_1] = FilteringByAvaraging(B,keplerian_signal_frequency,k_orbit_period,keplerian_perturbation.time./day);

%% COMPARISON
% Cartesian
figure
for n = 1:6
    subplot(3,2,n)
%     plot(cartesian_perturbation.time./day,A(:,n))
%     hold on
    
    plot(cartesian_perturbation.time./day,cartesian_signal_avaraged_1.signal_avaraged(:,n))
    hold on;
    plot(keplerian_perturbation.time./day,keplerian_signal_avaraged_1.signal_avaraged(:,n))

    legend('Cartesian', 'Keplerian');
    grid on;
    if n == 1
       ylabel('[km]')
    else if n == 2
           ylabel('') 
        else
           ylabel('deg [$\circ$]');
        end
    end
    xlabel('Time [day]')
    GraphTitle(n);
end


%% Read Data

hubble = ReadSatelliteData("HubbleData.csv");
%% Comparison with real data
% Our comparison satellite is the Hubble Space Telescope, because it has
% the same inclination and a very close perigee. Moreover it is affected by
% our perturbation, J2 effect and drag

% The orbit propagation starts on February, 1st 2015
HubbleSpaceTelescope.semimajor_axis = 6.927229749775316E+03;
HubbleSpaceTelescope.eccentricity = 3.523837283665754E-04;
HubbleSpaceTelescope.inclination = deg2rad(2.842339284695138E+01);
HubbleSpaceTelescope.perigee_altitude = 6.924788706728839E+03-astroConstants(23); 
HubbleSpaceTelescope.RAAN = deg2rad(3.316645765511934E+02); 
HubbleSpaceTelescope.argument_of_perigee = deg2rad(2.729869822059534E+02);
HubbleSpaceTelescope.true_anomaly = deg2rad(2.280687372976299E+01);
HubbleSpaceTelescope.period = 2*pi*sqrt(HubbleSpaceTelescope.semimajor_axis.^3/PlanetaryExplorerData.gravitational_constant);
HubbleSpaceTelescope.drag_coefficient = 2.2; 
HubbleSpaceTelescope.A_m_ratio = 13.2*4.2/11110; %data from Wikipedia

%Additional datas about Earth needed for the integration with the ode
%solver 
HubbleSpaceTelescope.gravitational_constant = astroConstants(13);
HubbleSpaceTelescope.omega_E = 2*pi/(23*3600+56*60)*[0;0;1];
HubbleSpaceTelescope.Earth_radius = astroConstants(23);
HubbleSpaceTelescope.J2 = 0.00108263;

%Comparison with keplerian integration 
n = 50;
[keplerian_perturbation] = PerturbingAccelerationKeplerian(HubbleSpaceTelescope, n);
time_keplerian = keplerian_perturbation.time ./ 86400;
 
[A_Hubble_kep] = ParametersMatrix (keplerian_perturbation.orbital_parameters);

a_Hubble_kep = A_Hubble_kep(:,1);
e_Hubble_kep = A_Hubble_kep(:,2);
i_Hubble_kep = rad2deg(A_Hubble_kep(:,3));
RAAN_Hubble_kep = rad2deg(A_Hubble_kep(:,4));
omeghino_Hubble_kep = rad2deg(A_Hubble_kep(:,5));
theta_Hubble_kep = rad2deg(A_Hubble_kep(:,6));

figure;
plot (time_keplerian,a_Hubble_kep)
grid on
hold on 
plot (time_keplerian,hubble.semimajor_axis (:,1))
title ('Hubble semimajor axis comparison')
legend ('Integr result','Given data')

figure;
plot (time_keplerian,e_Hubble_kep)
grid on
hold on 
plot (time_keplerian,hubble.eccentricity (:,1))
title ('Hubble eccentricity comparison')
legend ('Integr result','Given data')

figure;
plot (time_keplerian,i_Hubble_kep)
grid on
hold on 
plot (time_keplerian,rad2deg(hubble.inclination (:,1)))
title ('Hubble inclination comparison')
legend ('Integr result','Given data')

figure;
plot (time_keplerian,RAAN_Hubble_kep)
grid on
hold on 
plot (time_keplerian,rad2deg(hubble.omega (:,1)))
title ('Hubble right ascension of the ascending node comparison')
legend ('Integr result','Given data')

figure;
plot (time_keplerian,omeghino_Hubble_kep)
grid on
hold on 
plot (time_keplerian,rad2deg(hubble.omeghino(:,1)))
title ('Hubble argument of perigee comparison')
legend ('Integr result','Given data')

figure;
plot (time_keplerian,theta_Hubble_kep)
grid on
hold on 
plot (time_keplerian,rad2deg(hubble.semimajor_axis (:,1)))
title ('Hubble true anomaly comparison')
legend ('Integr result','Given data')

%%

%Comparison with cartesian integration 
n= 1*365;
[cartesian_perturbation] = PerturbingAccelerationCartesian(HubbleSpaceTelescope, n);
time_cartesian = cartesian_perturbation.time ./ 86400 + hubble.jd2000(1);

[A_Hubble_cart] = ParametersMatrix (cartesian_perturbation.orbital_parameters);

a_Hubble_cart = A_Hubble_cart(:,1);
e_Hubble_cart = A_Hubble_cart(:,2);
i_Hubble_cart = rad2deg(A_Hubble_cart(:,3));
RAAN_Hubble_cart = rad2deg(A_Hubble_cart(:,4));
omeghino_Hubble_cart = rad2deg(A_Hubble_cart(:,5));
theta_Hubble_cart = rad2deg(A_Hubble_cart(:,6));

%%
figure;
plot (time_cartesian,a_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.semimajor_axis (:,1))
ylabel('Semi-major axis [km]');
xlabel('JD2000');
legend ('Integrated result','Hubble Data')
grid minor;

figure;
plot (time_cartesian,e_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.eccentricity (:,1))
ylabel('Eccentricity [ ]');
xlabel('JD2000');
legend ('Integrated result','Hubble Data')
grid minor;

figure;
plot (time_cartesian,i_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.inclination (:,1))
ylabel('Inclincation [$^\circ$]');
xlabel('JD2000');
legend ('Integrated result','Hubble Data')
grid minor;

figure;
plot (time_cartesian,RAAN_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.omega (:,1))
ylabel('Right Ascension of Ascending Node [$^\circ$]');
xlabel('JD2000');
legend ('Integrated result','Hubble Data')
grid minor;

figure;
plot (time_cartesian,omeghino_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.omeghino(:,1))
ylabel('Argument of Perigee [$^\circ$]');
xlabel('JD2000');
legend ('Integrated result','GHubble Data')
grid minor;

figure;
plot (time_cartesian,theta_Hubble_cart)
grid on
hold on 
plot (hubble.jd2000,hubble.theta(:,1))
ylabel('True Anomaly [$^\circ$]');
xlabel('JD2000');
legend ('Integrated result','Hubble Data')
grid minor;