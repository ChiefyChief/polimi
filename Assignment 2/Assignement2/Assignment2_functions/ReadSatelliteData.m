function satellite_data = ReadSatelliteData(file_name)
% Function used to read and save the satellite parameters in a more
% handle way
%
% INPUT:
%       file_name        Source of the data of the satellite
%
% OUTPUT: 
%       satellite_data   Struct with all the data
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

table = readtable(file_name);

jd2000 = table2array(table(:,1));
eccentricity = table2array(table(:,3));
inclination = table2array(table(:,5));
omega = table2array(table(:,6));
omeghino = table2array(table(:,7));
theta = table2array(table(:,11));
semimajor_axis = table2array(table(:,12));

satellite_data.jd2000 = str2double(jd2000);
satellite_data.eccentricity = str2double(eccentricity);
satellite_data.inclination = str2double(inclination);
satellite_data.omega = str2double(omega);
satellite_data.omeghino = str2double(omeghino);
satellite_data.theta = str2double(theta);
satellite_data.semimajor_axis = str2double(semimajor_axis);

end