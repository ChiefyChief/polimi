function [odefun] = J2_2BP (mu_Earth)

% Handle function for the two-body problem equation of motion considering the secular J2 perturbation
%
% PROTOTYPE
% [odefun] = J2_2BP (mu_Earth)
%
% INPUTS
% mu_Earth      gravitation parameter of the main attractor [L^3/T^2]
% 
% OUTPUTS
% odefun        handle function to be integrated in the ODESolver
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
%
% functions needed : astroConstant

Re = astroConstants(23);
J2 = 0.00108263;

% y is a [6x1] vector with y(1),y(2),y(3) representing the components of
% the position vector and y(4),y(5),y(6) representing the components of the velocity vector
odefun = @(t,y) [y(4);
    y(5);
    y(6);
    -mu_Earth.*y(1)./(y(1).^2+y(2).^2+y(3).^2)^1.5+(1.5*J2*mu_Earth*Re.^2./(y(1).^2+y(2).^2+y(3).^2)^2)*(y(1)*(5*y(3).^2/(y(1).^2+y(2).^2+y(3).^2)-1)/(y(1).^2+y(2).^2+y(3).^2)^0.5);
    -mu_Earth.*y(2)./(y(1).^2+y(2).^2+y(3).^2)^1.5+(1.5*J2*mu_Earth*Re.^2./(y(1).^2+y(2).^2+y(3).^2)^2)*(y(2)*(5*y(3).^2/(y(1).^2+y(2).^2+y(3).^2)-1)/(y(1).^2+y(2).^2+y(3).^2)^0.5);
    -mu_Earth.*y(3)./(y(1).^2+y(2).^2+y(3).^2)^1.5+(1.5*J2*mu_Earth*Re.^2./(y(1).^2+y(2).^2+y(3).^2)^2)*(y(3)*(5*y(3).^2/(y(1).^2+y(2).^2+y(3).^2)-3)/(y(1).^2+y(2).^2+y(3).^2)^0.5)];
