function [t,y] = ODESolver(odefun,tspan,y0)

% Solver for ODE handle functions
%
% PROTOTYPE
% [t,y] = ODESolver(odefun,tspan,y0)
%
% INPUTS
%   odefun          handle function to be integrated
%   tspan [1x2]     contains the initial and final times of the integration [T]
%   y0 [6x1]        initial value vector
% 
% OUTPUT
%   t [Nx1]         vector containing all the time steps [T]
%   y [Nx6]         matrix containing all the result of the integration
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

% options for the ode113
options=odeset('RelTol',1e-13,'AbsTol',1e-14);

%result
[t,y]=ode113(odefun,tspan,y0,options);
