function k = CartesianPerturbation(arr, mu, planet_radius, J2, drag_parameters)
% Function to find the orbit pertubations in cartesian coordinates 
%
%
% INPUT
%   arr             array for position compones 
%   mu              gravitational parameter [L^3/T^2]
%   planet_radius   radius of the planet [L]
%   J2              given value related to the corresponding effect 
%   drag_parameters drag coefficient 
%
% OUTPUT
% k                 result of the integration 
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
x = arr(1);
y = arr(2);
z = arr(3);

r_vector = [x, y, z];

v_x = arr(4);
v_y = arr(5);
v_z = arr(6);

v = [v_x, v_y, v_z];

%% Calculate Drag Perturbation

% Calculate which atmospheric layer the craft is in
altitude = norm([x y z]) - planet_radius; 

[density_parameters] = DensityModel(altitude); 

reference_altitude = density_parameters.h_0; 
reference_density = density_parameters.rho_0; 
scale_height = density_parameters.H; 

a_m_ratio = drag_parameters.a_m_ratio;
cd = drag_parameters.cd;
omega_earth = drag_parameters.omega_earth;

atmospheric_drag_multiplier = -(1/2) * a_m_ratio * cd * reference_density * exp( -(altitude - reference_altitude) / scale_height);

v_relative = v - cross(omega_earth, r_vector);

drag = atmospheric_drag_multiplier .* (norm(v_relative)^2) .* v_relative ./ norm(v_relative);

%% Calculate J2 Perturbation

r = sqrt(x^2 + y^2 + z^2);

j2_coeff = (3/2) * J2 * mu * planet_radius^2 / r^4;

j2_x = j2_coeff * (x/r) * (5 * z^2 / r^2 - 1);
j2_y = j2_coeff * (y/r) * (5 * z^2 / r^2 - 1);
j2_z = j2_coeff * (z/r) * (5 * z^2 / r^2 - 3);


%% Calculate State Vector

p_x = j2_x + drag(1);
p_y = j2_y + drag(2);
p_z = j2_z + drag(3);

k = [
    v_x;
    v_y;
    v_z;
    - mu * x / r^3 + p_x;
    - mu * y / r^3 + p_y;
    - mu * z / r^3 + p_z;
    ];


                    
end    