function [G,f] = dft(g,t,Nf)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TRASFORMATA DISCRETA DI FOURIER 
% g = segnale da trasformare
% t = asse dei tempi
% Nf = numero dei punti in frequenza da calcolare
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The execution time for fft depends on the length of the transform. 
% The time is fastest for powers of two and almost as fast for lengths 
% that have only small prime factors. The time is typically several 
% times slower for lengths that are prime, or which have large prime factors.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not(exist('Nf'))
    Nf = length(t);
end

G = fft(g,Nf,1); % faccio sempre la trasformata lungo le colonne (se g � una matrice)
G = fftshift(G,1);

% asse delle frequenze
dt = t(2)-t(1);
if mod(Nf,2) % numero dispari di campioni
    f = [-(Nf-1)/2:(Nf-1)/2]/Nf/dt;
else   % numero pari di campioni
    f = [-Nf/2:Nf/2-1]/Nf/dt;
end

% % compensazione del ritardo della fft
% fase = -2*pi*f(:)*t(1);
% w = exp(1i*fase(:))*ones(1,size(g,2));
% G = G.*w;

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% esempio
clear, clc
t = linspace(-1,1,101);
g = randn(length(t),2) + 1i*randn(length(t),2);

Nf = 2^ceil(log2(length(t)));
[G,f] = dft(g,t,Nf);

W = exp(-1i*2*pi*f(:)*t);
G2 = W*g;

figure, 
subplot(2,1,1), plot(f,abs(G2-G)), grid

g2 = idft(G,f,t);
subplot(2,1,2), plot(t,abs(g2(1:length(t),:)-g)), grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
