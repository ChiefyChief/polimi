function [alpha,delta,Long,Lat] = GroundTrack(orbit,revolutions,perturbation, start_style, end_style)

% Plot of the ground track of a satellite onto Earth's surface given the 
% orbital parameters, the number of orbits and the perturbations

% PROTOTYPE
% [alpha,delta,Long,Lat] = GroundTrack(orbit,revolutions,perturbation)
%
% INPUTS
%   orbit               struct containing the following parameters:
%                           semimajor axis [L]
%                           eccentricity [-]
%                           inclination [rad]
%                           RAAN = right ascension of the ascending node [rad]
%                           argument of the perigee [rad]
%                           true anomaly [rad]
%                           orbital period [T]
%                           gravitational parameter of the main attractor [L^3/T^2]
%   revolutions [1]     number of satellite orbits to be plotted
%   perturbation        perturbing effect acting on the satellite:
%                           'none' = no perturbation
%                           'J2' = secular J2 perturbation
%
% OUTPUT
%   alpha [Nx1]         right ascension vector [rad]
%   delta [Nx1]         declination vector [rad]
%   Long  [Nx1]         longitude vector [deg]
%   Lat   [Nx1]         latitude vector [deg]
% 
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
%
% Functions needed
%   Keplerian2CartesianOrbit
%   Unperturbed2BP
%   J2_2BP
%   ODESolver

% Computation of the initial point
[r0,v0] = Keplerian2CartesianOrbit(orbit,orbit.gravitational_constant);

% Initial vector
y0 = [r0;v0];

% Orbit position and velocity from the initial vector considering the
% perturbing effect acting on the satellite
if strcmp(perturbation,'none') == 1
    [odefun] = Unperturbed2BP (orbit.gravitational_constant);
    [t,r] = ODESolver(odefun,[0 revolutions*orbit.period],y0);
elseif strcmp(perturbation,'J2') == 1
    [odefun] = J2_2BP (orbit.gravitational_constant);
    [t,r] = ODESolver(odefun,[0 revolutions*orbit.period],y0);
end

% 
delta = zeros(length(t),1);
alpha = zeros(length(t),1);
long = zeros(length(t),1);
lat = zeros(length(t),1);
thetag = zeros(length(t),1); %thetaGreenwich, that starts from 0
rr = zeros(length(t),1); %magnitude of the position vector

% Calculation for every time step
for j = 1:length(t)
    rr(j) = norm(r(j,1:3));
    delta(j) = asin(r(j,3)/rr(j));
    if r(j,2)/rr(j) > 0
    alpha(j) = acos((r(j,1)/rr(j))./cos(delta(j)));
    else
    alpha(j) = 2*pi-acos((r(j,1)/rr(j))./cos(delta(j)));
    end
    thetag(j) = 2*pi/(23*3600+56*60)*(t(j)-0);
    long(j) = wrapToPi(alpha(j)-thetag(j));
    lat(j) = delta(j);
end

% Plot
% Finding of the time step of angle change
A=[];
for j = 2:length(t)
        if abs(long(j)-long(j-1)) > pi
            A = [A;j];
        end 
end

% Plot with NaN: every time step of angle change a NaN is going to be
% introduced inside the Long and Lat vectors
if isempty(A) == 1
    Long(1:length(t),1) = long;
    Lat(1:length(t),1) = lat;
else
    Long = zeros((length(t)+length(A)),1);
    Lat = zeros((length(t)+length(A)),1);
    for j = 1:length(A)
    if j <= 1
        k = A(j);
        Long(1:k-1) = long(1:k-1);
        Long(k) = nan;
        Long(k+1:(end-length(A)+j)) = long(k:end);
        Lat(1:k-1) = lat(1:k-1);
        Lat(k) = nan;
        Lat(k+1:(end-length(A)+j)) = lat(k:end);
    else
        k = A(j);
        Long(1:k-2+j) = Long(1:k-2+j);
        Long(k+j-1) = nan;
        Long((k+j):end-length(A)+j) = long(k:end);
        Lat(1:k-2+j) = Lat(1:k-2+j);
        Lat(k+j-1) = nan;
        Lat((k+j):end-length(A)+j) = lat(k:end);
    end
    end
end

% Long and Lat in degree
Long = rad2deg(Long);
Lat = rad2deg(Lat);

set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');

% Plot of the ground track
if strcmp(perturbation,'none') == 1
    plot(Long,Lat,'LineWidth',1.5)
elseif strcmp(perturbation,'J2') == 1
    plot(Long,Lat,'r','LineWidth',1.5)
end
 
hold on
% Plot of the initial and final positions
plot(Long(1),Lat(1), start_style,Long(end),Lat(end),end_style,'LineWidth',2);

xlim([-180,180])
ylim([-90,90])
xlabel('Longitude [Deg]')
ylabel('Latitude [Deg]')
