function [r,v] = Keplerian2CartesianOrbit(orbit,mu)

% Computation of the cartesian coordinates (r and v) starting from the orbital parameters
%
% PROTOTYPE
% [r,v] = Keplerian2CartesianOrbit(orbit,k)
%
% INPUT
% orbit[1x6]    structure containing the orbital parameters
% k[1]          gravitational parameter of the main attractor [L^3/T^2]
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

a = orbit.semimajor_axis;
e = orbit.eccentricity;
OMEGA = orbit.RAAN;
i = orbit.inclination;
omega = orbit.argument_of_perigee;
theta = orbit.true_anomaly;

% Rotation matrices
R1 = [cos(OMEGA)     sin(OMEGA)    0
     -sin(OMEGA)    cos(OMEGA)     0
         0              0          1];

R2 = [1     0       0
      0   cos(i)  sin(i)
      0  -sin(i)  cos(i)];

R3 = [cos(omega)   sin(omega)   0
     -sin(omega)  cos(omega)    0
          0            0        1];

% Compute r_peri and v_peri in the perifocal frame
p = a*(1-e.^2);
h = sqrt(mu*p);
r_peri = (h.^2./mu)*1./(1+e*cos(theta))*[cos(theta);sin(theta);0];
v_peri = mu./h*[-sin(theta);(e+cos(theta));0];

% Compute of r and v
r = (R1'*(R2'*(R3'*r_peri)));
v = (R1'*(R2'*(R3'*v_peri)));
