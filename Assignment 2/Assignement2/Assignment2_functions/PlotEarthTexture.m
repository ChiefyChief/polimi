function PlotEarthTexture
% Plot of Earth's Texture
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

image = imread('Earth','jpg');
imagesc(image);
imagesc([-180 180],[90 -90],image);
axis xy
grid on
hold on