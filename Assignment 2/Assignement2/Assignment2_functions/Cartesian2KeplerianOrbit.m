function [orbit] = Cartesian2KeplerianOrbit(r,v,mu)
% Function to find the orbital parameters starting from position and
% velocity vectors
%
% PROTOTYPE
% [orbit] = Cartesian2KeplerianOrbit(r,v,mu)
%
% INPUT
%   r [3x1]         position vector [L]
%   v [3x1]         velocity vector [L/T]
%   mu [1]          gravitational parameter [L^3/T^2]
%
% OUTPUT
%   orbit           struct with the orbital parameters
%                       semimajor axis [L]
%                       eccentricity [-]
%                       inclination [rad]
%                       right ascension of the ascending node [rad]
%                       argument of the perigee [rad]
%                       true anomaly [rad]
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

% Unitary vectors
e_x = [1;0;0];
e_z = [0;0;1];

% Compute of the magnitude of r and v vectors and of the radial velocity
r_mag = norm(r);
v_mag = norm(v);
v_rad = dot(r,v)/r_mag;

% Compute h and inclination
h = cross(r,v);
h_mag = norm(h);

i = acos(dot(h,e_z)/h_mag);

% Compute N and omega
N = cross(e_z,h)/h_mag;
N_mag = norm(N);

if N(2) >= 0
    omega = acos(dot(e_x,N)/N_mag);
elseif N(2) < 0
    omega=2*pi-acos(dot(e_x,N)/N_mag);
elseif i == 0 || i == pi %singularity if the orbit is not inclined
    omega = 0;
    N = e_x;
    N_mag = norm(N);
end

% Compute eccentricity, semimajor axis and argument of perigee
ecc = cross(v,h)/mu-r/r_mag; %ecc = eccentricity vector
e = norm(ecc); %e = magnitude of ecc
a = (h_mag^2/mu)./(1-e^2);

if ecc(3) >= 0
    omeghino = acos(dot(N,ecc)/(N_mag*e));
elseif ecc(3) < 0
    omeghino = 2*pi-acos(dot(N,ecc)/(N_mag*e));
elseif e == 0 %singularity if orbit is circular
    omeghino = 0;
end

% Compute of theta
v_peri = mu/h_mag*(1+e);
if v_rad > 0
    theta = acos(dot(r,ecc)/(e*r_mag));
elseif v_rad < 0
    theta = 2*pi-acos(dot(r,ecc)/(e*r_mag));
elseif v_rad == 0 && v_mag == v_peri %singularity if theta is at the periapsis
    theta = 0;
elseif v_rad == 0 && v_mag ~= v_peri %singularity if theta is at the apoapsis
    theta = pi;
end

orbit.semimajor_axis = a;
orbit.eccentricity = e;
orbit.inclination = i;
orbit.RAAN = omega;
orbit.argument_of_perigee = omeghino;
orbit.true_anomaly = theta;
end
