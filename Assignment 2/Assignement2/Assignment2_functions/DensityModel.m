function [DensityParameters] = DensityModel(h) 

%INPUTS
%h [Km] = actual altitude 

%OUTPUT
%DensityParameters = strcuture with fields
%                    h_0 = reference altitude in each layer [Km]
%                    rho_0 = reference density in each layer [Kg/m^3]
%                    H = scale height in each layer [Km]  

% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

    if (h>0 && h<=25)
       DensityParameters.h_0 = 0;
       DensityParameters.rho_0 = 1.225;
       DensityParameters.H = 7.249;
    elseif (h>25 && h<=30)
       DensityParameters.h_0 = 25;
       DensityParameters.rho_0 = 3.899e-2;
       DensityParameters.H = 6.349; 
    elseif (h>30 && h<=40)
       DensityParameters.h_0 = 30;
       DensityParameters.rho_0 = 1.774e-2;
       DensityParameters.H = 6.682; 
    elseif (h>40 && h<=50)
       DensityParameters.h_0 = 40;
       DensityParameters.rho_0 = 3.972e-3;
       DensityParameters.H = 7.554;
    elseif (h>50 && h<=60)
       DensityParameters.h_0 = 50;
       DensityParameters.rho_0 = 1.057e-3;
       DensityParameters.H = 8.382; 
    elseif (h>60 && h<=70)
       DensityParameters.h_0 = 60;
       DensityParameters.rho_0 = 3.206e-4;
       DensityParameters.H = 7.714; 
    elseif (h>70 && h<=80)
       DensityParameters.h_0 = 70;
       DensityParameters.rho_0 = 8.770e-5;
       DensityParameters.H = 6.549;
    elseif (h>80 && h<=90)
       DensityParameters.h_0 = 80;
       DensityParameters.rho_0 = 1.905e-5;
       DensityParameters.H = 5.799; 
    elseif (h>90 && h<=100)
       DensityParameters.h_0 = 90;
       DensityParameters.rho_0 = 3.396e-6;
       DensityParameters.H = 5.382; 
    elseif (h>100 && h<=110)
       DensityParameters.h_0 = 100;
       DensityParameters.rho_0 = 5.297e-7;
       DensityParameters.H = 5.877; 
    elseif (h>110 && h<=120)
       DensityParameters.h_0 = 110;
       DensityParameters.rho_0 = 9.661e-8;
       DensityParameters.H = 7.263; 
    elseif (h>120 && h<=130)
       DensityParameters.h_0 = 120;
       DensityParameters.rho_0 = 2.438e-8;
       DensityParameters.H = 9.473;
    elseif (h>130 && h<=140)
       DensityParameters.h_0 = 130;
       DensityParameters.rho_0 = 8.484e-9;
       DensityParameters.H = 12.636;
    elseif (h>140 && h<=150)
       DensityParameters.h_0 = 140;
       DensityParameters.rho_0 = 3.845e-9;
       DensityParameters.H = 16.149; 
    elseif (h>150 && h<=180)
       DensityParameters.h_0 = 150;
       DensityParameters.rho_0 = 2.070e-9;
       DensityParameters.H = 22.523;
    elseif (h>180 && h<=200)
       DensityParameters.h_0 = 180;
       DensityParameters.rho_0 = 5.464e-10;
       DensityParameters.H = 29.740; 
    elseif (h>200 && h<=250)
       DensityParameters.h_0 = 200;
       DensityParameters.rho_0 = 2.789e-10;
       DensityParameters.H = 37.105;
    elseif (h>250 && h<=300)
       DensityParameters.h_0 = 250;
       DensityParameters.rho_0 = 7.248e-11;
       DensityParameters.H = 45.546; 
    elseif (h>300 && h<=350)
       DensityParameters.h_0 = 300;
       DensityParameters.rho_0 = 2.418e-11;
       DensityParameters.H = 53.628;
    elseif (h>350 && h<=400)
       DensityParameters.h_0 = 350;
       DensityParameters.rho_0 = 9.158e-12;
       DensityParameters.H = 53.298; 
    elseif (h>400 && h<=450)
       DensityParameters.h_0 = 400;
       DensityParameters.rho_0 = 3.725e-12;
       DensityParameters.H = 58.515;
    elseif (h>450 && h<=500)
       DensityParameters.h_0 = 450;
       DensityParameters.rho_0 = 1.585e-12;
       DensityParameters.H = 60.828; 
    elseif (h>500 && h<=600)
       DensityParameters.h_0 = 500;
       DensityParameters.rho_0 = 6.967e-13;
       DensityParameters.H = 63.822; 
    elseif (h>600 && h<=700)
       DensityParameters.h_0 = 600;
       DensityParameters.rho_0 = 1.544e-13;
       DensityParameters.H = 71.835; 
    elseif (h>700 && h<=800)
       DensityParameters.h_0 = 700;
       DensityParameters.rho_0 = 3.614e-14;
       DensityParameters.H = 88.667;
    elseif (h>800 && h<=900)
       DensityParameters.h_0 = 800;
       DensityParameters.rho_0 = 1.170e-14;
       DensityParameters.H = 124.64; 
    elseif (h>900 && h<=1000)
       DensityParameters.h_0 = 900;
       DensityParameters.rho_0 = 5.245e-15;
       DensityParameters.H = 181.05;
    elseif (h>1000)
       DensityParameters.h_0 = 1000;
       DensityParameters.rho_0 = 3.019e-15;
       DensityParameters.H = 268.00; 
    else
        disp("ERROR | Height: " + h);
    end