function [G, f] = SpectralAnalysis(time, A, N)
% Function that evaluate the spectra of a given signal. The signal is given
% in the time domain and converted in the frequencies domain inside the
% function.
%
% INPUT
% time [1xn]        vector containing all the time of observation
% A [nx1]           vector containing the parameters we want to transform
% N [1]             column index, needed for the plot title
%
% OUTPUT
% G [nx1]           vector containing the parameter in the frequency domain
% f [1xn]           vector containing the frequencies of the domain
%                   it gives the frequency spectrum of the parameter in input
%
% Functions needed
% dft               needed to pass from the time domain to the frequency
%                   domain
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

[G, f] = dft(A,time);
L = length(G);
G = abs(G/L);

n = length(f);
k = 15;
f_zoom = f(n/2-k:n/2+k);
G_zoom = G(n/2-k:n/2+k,1);

stem(f_zoom,G_zoom)
grid on
xlabel('f(Hz)');
ylabel('Amplitude');

return
