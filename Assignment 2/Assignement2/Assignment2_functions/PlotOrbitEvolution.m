function PlotOrbitEvolution(r,counter,A,n)
% Plot of the orbit evolution
% 
% PROTOTYPE
% function PlotOrbitEvolution(r,counter,A,n)
%
% INPUT
% r [3xN]           position vector for every time step [L]
% counter [1xM]     counter of number of orbits made
% A [1xP]           vector containing all the time steps when a new orbit
%                   starts
% n [1]             represents the specified revolution in which we plot
%                   the orbit
% OUTPUT
%
% Image needed:
%               Earth.jpg
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

A = [2;A];

if nargin == 3
    % Evaluation for every time step
    for j = 2:counter
        % plot of Earth
        [X,Y,Z] = ellipsoid(0,0,0,6378,6378,6357);
        s = surf(X,Y,-Z,'FaceColor','None','EdgeColor','None');
        image = imread('Earth','jpg');
        set(s,'FaceColor','texturemap','CData',image);
    
        % plot setting
        axis equal
        grid on
        hold on
        xlim([-50000,50000])
        ylim([-50000,50000])
        zlim([-30000,30000])
        xlabel('rx [Km]','Color','k')
        ylabel('ry [Km]','Color','k')
        zlabel('rz [Km]','Color','k')
        title('Orbit evolution')

        % plot of the orbits
        plot3(r(1,(A(j-1)-1):A(j)),r(2,(A(j-1)-1):A(j)),r(3,(A(j-1)-1):A(j)),'LineWidth',3);
        vectarrow([0 0 0],r(:,A(j))) % eccentricity vector
        %plot3([0,10000],[0,0],[0,0],'k','LineWidth',2) % x axis
        %plot3([0,0],[0,10000],[0,0],'k','LineWidth',2) % y axis
        %plot3([0,0],[0,0],[0,10000],'k','LineWidth',2) % z axis
        %legend('Earth','Orbit')%,'Color','w','TextColor','k')
        drawnow limitrate
        hold off
    end
elseif nargin == 4
hold on;
    j = n;
        % plot of the orbits
        plot3(r(1,(A(j-1)-1):A(j)),r(2,(A(j-1)-1):A(j)),r(3,(A(j-1)-1):A(j)));
     %   vectarrow([0 0 0],r(:,A(j))) % eccentricity vector
        %plot3([0,10000],[0,0],[0,0],'k','LineWidth',2) % x axis
        %plot3([0,0],[0,10000],[0,0],'k','LineWidth',2) % y axis
        %plot3([0,0],[0,0],[0,10000],'k','LineWidth',2) % z axis
        %legend('Earth','Orbit')%,'Color','w','TextColor','k')
end