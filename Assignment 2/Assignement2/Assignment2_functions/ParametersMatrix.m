function [A] = ParametersMatrix (parameters)

% INPUT
% parameters - part of the struct with all the variations evaluated before

%OUTPUT
% A - Matrix with all parameters per columns

% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

A = zeros(size(parameters,2),6);

for i = 1:size(parameters,2)
    A(i,1) = parameters(i).semimajor_axis;
    A(i,2) = parameters(i).eccentricity;
    A(i,3) = parameters(i).inclination;
    A(i,4) = parameters(i).RAAN;
    A(i,5) = parameters(i).argument_of_perigee;
    A(i,6) = parameters(i).true_anomaly;
end
