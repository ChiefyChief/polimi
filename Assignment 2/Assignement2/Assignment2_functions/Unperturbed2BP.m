function [odefun] = Unperturbed2BP (mu_Earth)
% Handle function for the unperturbed two-body problem equation of motion 
% considering the secular J2 perturbation
% 
% PROTOTYPE
% [odefun] = J2_2BP (mu_Earth)
%
% INPUTS
% mu_Earth      gravitation parameter of the main attractor [L^3/T^2]
% 
% OUTPUTS
% odefun        handle function to be integrated in the ODESolver
%
% y is a [6x1] vector with y(1),y(2),y(3) representing the components of
% the position vector and y(4),y(5),y(6) representing the components of the velocity vector
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

odefun=@(t,y) [y(4);
    y(5);
    y(6);
    -mu_Earth.*y(1)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -mu_Earth.*y(2)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -mu_Earth.*y(3)./(y(1).^2+y(2).^2+y(3).^2)^1.5];
