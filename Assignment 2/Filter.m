function [Y] = Filter (B, f_0, f, N)
% The function takes as imput a signal which is already in the frequency
% domain and filter it on a specific frequency given as input parameter.
% The filter choosen in this case is a box filter.
%
% INPUT
% signal [nx1]      vector containig all the value of the parameter we want to
%                   filter.
% f_0 [1]           frequency on which we want to filter the signal.
% f [1xn]           vector containing all the frequencies coming from the Fourier (dft)
% N [1]             column index, needed for the plot title.
%
% OUTPUT
% Y [n,1]           filtered signal;
%                   We obtain the graph of the filtered signal.
%
% Functions needed
% rectpuls

n=max(max(abs(B)));
m=f_0/1000;

% rectpuls is the box shaped filter
H=rectpuls(f./100000,m);
H=H(:);
Y=H.*B;
Y = abs(Y);

stem(f,abs(Y))
xlabel('frequenza [Hz]')
ylabel('abs(Y)')

switch N
    case 1
        title('Semimajor axis spectrum filtered')
    case 2
        title('Eccentricity spectrum filtered')
    case 3 
        title('Inclination spectrum filtered')
    case 4
        title('Rigth ascension of the ascending node spectrum filtered')
    case 5
        title('Argument of perigee spectrum filtered')
    case 6
        title('True anomaly spectrum filtered')
end

