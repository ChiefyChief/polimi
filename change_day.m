function [window_date] = change_day (t0,tf)

count_day = 0;
[bis, bis_years] = bisestile_check(t0,tf);
window_month = change_month (t0,tf);
        
for i = 2:length(window_month)-1
    count_day = count_day + day_month(window_month(i,2));
    if window_month(i,2) == 02
        for yr = 1:length(bis_years)
            if window_month(i,1) == bis_years(yr)
                count_day = count_day + 1; % we are adding the 29
            end
        end
    end
end

% Initial date
in_bis = 0;
if bis(1) && t0(1) == bis_years(1)
    count_day = count_day + (29-t0(3));
    in_bis = 1;
else
    count_day = count_day + (day_month(t0(2))-t0(3));
end
% Final date
fin_bis = 0;
if bis(2) && tf(1) == bis_years(end)
    count_day = count_day + (tf(3));
    fin_bis = 1;
else
    count_day = count_day + tf(3);
end

window_date = zeros(count_day,3);
window_date(1:end-1,1) = t0(1);
window_date(end,1) = tf(1);
window_date(1:end-1,2) = t0(2);
window_date(1,3) = t0(3);
window_date(end,2) = tf(2);
window_date(end,3) = tf(3);

% First month
n_day = day_month(t0(2));
if in_bis
    n_day = n_day + 1;
end
i = 2;
while (window_date(i-1,3)<=n_day)
    window_date(i,3) = window_date(i-1,3)+1;
    i = i+1;
end
window_date(i-1:end-1,2) = t0(2)+1;
window_date(i-1,3) = 1;

for mt = 2:length(window_month)
    n_day = day_month(window_month(mt,2));
    if window_month(mt,2) == 2
        for yr = 1:length(bis_years)
            if window_month(mt,1) == bis_years(yr)
                n_day = n_day + 1; % we are adding the 29
            end
        end
    end
    while i<=count_day
        while (window_date(i-1,3)<=n_day)
            window_date(i,3) = window_date(i-1,3) + 1;
            i = i+1;
        end
        window_date(i-1,3) = 1;
        window_date(i-1:end,2) = window_date (i-1,2)+1;
        if window_date(i-1,2) == 13
            window_date(i-1:end,1) = window_date(i-1,1)+1;
            window_date(i-1:end,2) = 1;
        end
    end
end
    
            
    