%{
    Interplanetary Explorer Mission Analysis
    
    Group 10

    Authors:
    - Bonariol Teodoro          941028
    - Cantoni Alexia            945126
    - Capelli Anita             939522
    - Parisi Adrian Burton      940789
%}

clear variables;
clc;
close all;


% Add paths to third party common functions
addpath('Assignment1_functions');

%% Setup

max_iteration = 4999;

% NOTE - 14610 days between the dates [ 1 1 2020 ] and [ 1 1 2060 ]

complete_time_window = WindowCreation([ 2020 1 1 ], [ 2060 1 1 ]);

sparse_matrix_dimension = length(complete_time_window);

%% Synodic period

% Inside the function there are two if cases, one of them is if 0 thus
% whenever the code is runned it is not taken in consideration. If the
% analysis wants to be lead in a more precised way will be neccesary to
% change those conditions
[synodic_period.dates] = SynodicPeriod3Planets(complete_time_window,5,3,2);
 
if 0
% Evaluation of the period in years
synodic_period.period = size(WindowCreation(synodic_period.dates(1,1:3),synodic_period.dates(2,1:3)),1);
synodic_period.period = synodic_period.period/365; % we evaluate it in years with respet to the standard number of years
end

single_index.departure = 5430;
single_index.flyby = 6344;
single_index.arrival = 6831;

%% Compute

% Monte Carlo Magic

complete_run_count = 0;
crashed_count = 0;

loop_count = 0;
file_appends = 0;
graphs_saved = 0;

debug_output = false;

latest_date.departure = '';
latest_date.flyby = '';
latest_date.arrival = '';
latest_gradient_steps = 0;
latest_delta_v = 0;
latest_crashed = false;

minimum_date_output.departure = '';
minimum_date_output.flyby = '';
minimum_date_output.arrival = '';
minimum_gradient_steps = 0;
minimum_delta_v = inf;
minimum_crashed = false;

run_optimiser = true;
run_single_value = false;

if run_single_value 
    [ stencil ] = GenerateStencil(complete_time_window, single_index);
    [ converged, minimum_index, delta_v, time_of_flight, crashed, power_ga ] = GradientStep(stencil);
    file_string = "DeltaV_" + delta_v + "_crashed_" + crashed + "_Index_" + minimum_index.departure + "_" + minimum_index.flyby + "_" + minimum_index.arrival;
            
    dates.departure = stencil.central.date.departure;
    dates.flyby = stencil.central.date.flyby;
    dates.arrival = stencil.central.date.arrival;
           
    orbits.departure = stencil.central.orbit.departure;
    orbits.flyby = stencil.central.orbit.flyby;
    orbits.arrival = stencil.central.orbit.arrival;  
                        
    minimum_dates.departure = dates.departure;
    minimum_dates.flyby = dates.flyby;
    minimum_dates.arrival = dates.arrival;
    minimum_power_ga = power_ga;
                
    PlotInterplanetaryTransferWithGravityAssist(...
                    dates, ...
                    orbits, ...
                    file_string);
end

smaller = false;

while complete_run_count < 1 && run_optimiser

    index.departure = randi([1 sparse_matrix_dimension], 1, 1);
    index.flyby = randi([1 sparse_matrix_dimension], 1, 1);
    index.arrival = randi([1 sparse_matrix_dimension], 1, 1);

    if (index.departure < index.flyby) && (index.flyby < index.arrival)
        
        converged = false;
        current_iteration = 0;
                
        while ~converged
            loop_count = loop_count + 1;
            
            if mod(loop_count, 20) == 0
                clc;
                disp("---------------------------------");
                disp("Global loop count: " + loop_count);
                disp("Successful runs: " + complete_run_count);
                disp("Current Iteration: " + current_iteration);
                disp("Current Crashed Count: " + crashed_count);
                disp("Survived Count: " + (loop_count - crashed_count - 1));
                disp("Survived Fraction: " + ((loop_count - crashed_count - 1) / loop_count));
                disp("File Appends: " + file_appends);
                disp("Graphs Saved: " + graphs_saved);
                disp("---------------------------------");
                disp("Latest Values");
                disp("Delta V: " + latest_delta_v);
                disp("Departure Date: " + latest_date.departure);
                disp("Flyby Date: " + latest_date.flyby);
                disp("Arrival Date: " + latest_date.arrival);
                disp("Gradient Steps: " + latest_gradient_steps);
                disp("Crashed: " + latest_crashed);
                disp("---------------------------------");
                disp("Current Minimum Values");
                disp("Delta V: " + minimum_delta_v);
                disp("Departure Date: " + minimum_date_output.departure);
                disp("Flyby Date: " + minimum_date_output.flyby);
                disp("Arrival Date: " + minimum_date_output.arrival);
                disp("Gradient Steps: " + minimum_gradient_steps);
                disp("Crashed: " + minimum_crashed);
                disp("---------------------------------");
            end
            
            [ stencil ] = GenerateStencil(complete_time_window, index);
            [ converged, index, delta_v, time_of_flight, crashed, power_ga ] = GradientStep(stencil);
            
            current_iteration = current_iteration + 1;
            
            if crashed
                crashed_count = crashed_count + 1;
                break;
            end
            
            orbits.departure = stencil.central.orbit.departure;
            orbits.flyby = stencil.central.orbit.flyby;
            orbits.arrival = stencil.central.orbit.arrival;
            
            dates.departure = stencil.central.date.departure;
            dates.flyby = stencil.central.date.flyby;
            dates.arrival = stencil.central.date.arrival;
            
            file_string = "DeltaV_" + delta_v + "_crashed_" + crashed + "_Index_" + index.departure + "_" + index.flyby + "_" + index.arrival;
            
            latest_delta_v = delta_v;
            latest_date.departure = num2str(complete_time_window(index.departure,:));
            latest_date.flyby = "    " + num2str(complete_time_window(index.flyby,:));
            latest_date.arrival = "  " + num2str(complete_time_window(index.arrival,:));
            latest_gradient_steps = current_iteration;
            latest_crashed = crashed;
            
            if (latest_delta_v < minimum_delta_v) && ~crashed
                minimum_delta_v = delta_v;
                minimum_date_output.departure = num2str(complete_time_window(index.departure,:));
                minimum_date_output.flyby = "    " + num2str(complete_time_window(index.flyby,:));
                minimum_date_output.arrival = "  " + num2str(complete_time_window(index.arrival,:));
                minimum_gradient_steps = current_iteration;
                minimum_crashed = crashed;
                
                minimum_orbit.departure = orbits.departure;
                minimum_orbit.flyby = orbits.flyby;
                minimum_orbit.arrival = orbits.arrival;
                
                minimum_dates.departure = dates.departure;
                minimum_dates.flyby = dates.flyby;
                minimum_dates.arrival = dates.arrival;
                minimum_power_ga = power_ga;
                
                minimum_index.departure = index.departure;
                minimum_index.flyby = index.flyby;
                minimum_index.arrival = index.arrival;
                
                smaller = true;
            end 

            if (mod(current_iteration, 10) == 0 || converged) && debug_output && ~crashed && smaller
                smaller = false;
                PlotInterplanetaryTransferWithGravityAssist(...
                    dates, ...
                    orbits, ...
                    file_string);
                graphs_saved = graphs_saved + 1;
            end   
            
            if (mod(current_iteration, 5) == 0 || converged) && debug_output
                file_appends = file_appends + 1;
                output = [delta_v, time_of_flight, converged, crashed + 2, index.departure, index.flyby, index.arrival];
                dlmwrite('../../LabPlot/InterplanetaryTransferWithGravityAssist.csv',output,'delimiter',',','-append');
            end
            
            %{
                This is just a sanity loop break just in case the code hangs
                or gets stuck. 
            %}
            if current_iteration > max_iteration
                break;
            end
        end

        if converged
            if ~crashed
                complete_run_count = complete_run_count + 1;           
            end
        else
            continue;
        end    

    end
    
end

disp("Completed Optimisation");
disp("---------------------------------");

%% Pork Chop Plot Data generation
if false
disp("Generating Data for Pork Chop plots");

n = 365;

first_leg_dates.departure = complete_time_window((minimum_index.departure-n):(minimum_index.departure+n),:);
first_leg_dates.flyby = complete_time_window((minimum_index.flyby-n):(minimum_index.flyby+n),:);

second_leg_dates.flyby = complete_time_window((minimum_index.flyby-n):(minimum_index.flyby+n),:);
second_leg_dates.arrival = complete_time_window((minimum_index.arrival-n):(minimum_index.arrival+n),:);

%% Pork Chop Plot Calculation

disp("Calculating Data for Pork Chop plots");

first_leg_matrix_delta_v = NaN(length(first_leg_dates.departure), length(first_leg_dates.flyby));
second_leg_matrix_delta_v = NaN(length(second_leg_dates.flyby), length(second_leg_dates.arrival));

first_leg_matrix_tof = NaN(length(first_leg_dates.departure), length(first_leg_dates.flyby));
second_leg_matrix_tof = NaN(length(second_leg_dates.flyby), length(second_leg_dates.arrival));

mu = astroConstants(4);

for i = 1 : length(first_leg_dates.departure(:,1))
   
     clc;
     disp("---------------------------------");
     disp("Completed Fraction: " + i/length(first_leg_dates.departure));
     disp("---------------------------------");
     
    for j = 1 : length(first_leg_dates.flyby(:,1))
        
        first_departure = first_leg_dates.departure(i, :);
        first_flyby = first_leg_dates.flyby(j, :);
    
        second_flyby = second_leg_dates.flyby(i, :);
        second_arrival = second_leg_dates.arrival(j,:);
    
        orbit.first_departure = InitialisingPlanets(first_departure, 5);
        orbit.first_flyby = InitialisingPlanets(first_flyby, 3);
    
        orbit.second_flyby = InitialisingPlanets(second_flyby, 3);
        orbit.second_arrival = InitialisingPlanets(second_arrival, 2);
        
        first_leg_tof = 24 * 3600 * (date2mjd2000(first_flyby) - date2mjd2000(first_departure));
        second_leg_tof = 24 * 3600 * (date2mjd2000(second_arrival) - date2mjd2000(second_flyby));
    
        if (first_leg_tof > 0 && second_leg_tof > 0)
    
            first_leg_velocity = Lambert(orbit.first_departure, orbit.first_flyby, first_leg_tof, mu);
            second_leg_velocity = Lambert(orbit.second_flyby, orbit.second_arrival, second_leg_tof, mu);
               
            if abs(norm(first_leg_velocity.departure)) < 100
                first_leg_matrix_delta_v(i,j) = abs(norm(first_leg_velocity.departure));
            end
         
            if abs(norm(second_leg_velocity.arrival)) < 100
                second_leg_matrix_delta_v(i,j) = abs(norm(second_leg_velocity.arrival));
            end
            
            first_leg_matrix_tof(i,j) = first_leg_tof;
            second_leg_matrix_tof(i,j) = second_leg_tof;
            
        end
    end
end
end
%% Post-Process

% Fly-by plot
PlotHyperbola(power_ga);

