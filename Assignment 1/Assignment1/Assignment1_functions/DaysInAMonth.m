function [days] = DaysInAMonth (month)
% Returns the number of days in a given month
%
% INPUTS
%   month The numeric value of the month to check
%
% OUTPUTS
%   days The number of days in that month
%
% VERSIONS
%   28/11/2019  First version

switch (month)
    case {11,04,06,09}
        days = 30;
    case (02)
        days = 28;
    case {01,03,05,07,08,10,12}
        days = 31;
end
return