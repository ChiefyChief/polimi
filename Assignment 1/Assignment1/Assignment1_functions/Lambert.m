function [velocity] = Lambert(departure_orbit, arrival_orbit, time_of_flight, mu)
% Computation of the deltaVs for a transfer between the planets given the
% time of flight
%
% PROTOTYPE
% [velocity] = Lambert(departure_orbit, arrival_orbit, time_of_flight,k)
%
% INPUTS
%   departure_orbit     structure containing the orbital parameters of the departure orbit
%   arrival_orbit       structure containing the orbital parameters of the arrival orbit
%   time_of_flight[1]   time of flight of the transfer trajectory [T]
%   k[1]                gravitational parameter of the main attractor [L^3/T^2]
%
% OUTPUTS
%   velocity            structure containing:
%                           velocity.departure = required departure deltaV
%                           velocity.arrival = required arrival deltaV
%                           velocity.delta_v = total deltaV required
%
% VERSIONS
% 28/11/2019    First version
%
% Functions needed
%   Keplerian2CartesianOrbit
%   lambertMR

    % computation of departure and arrival position
    [r1, v1] = Keplerian2CartesianOrbit(departure_orbit, mu);
    [r2, v2] = Keplerian2CartesianOrbit(arrival_orbit, mu);

    % computation of the transfer orbit's required deltaV
    [~,~,~,~,VI,VF,~,~] = lambertMR(r1, r2, time_of_flight, mu, 0, 0, 0, 2);

    % computation of the outputs
    velocity.departure = VI'-v1;
    velocity.departure_raw = VI';
    
    velocity.arrival = v2-VF';
    velocity.arrival_raw = VF';
    
    velocity.delta_v = norm(velocity.departure) + norm(velocity.arrival);
    
end