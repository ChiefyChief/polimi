function [struct] = PlotHyperbola (struct)
% Function to plot the asymmetric hyperbolic trajectory of the fly-by around Earth
%
% PROTOTYPE
% [struct] = PlotHyperbola (struct)
%
% INPUTS
%   struct      containing the following parameters coming from the calculation
%               of the powered gravity assist:
%                   v_minus [3x1]       velocity at infinitive in the incoming 
%                                       leg of the hyperbola [L/T] 
%                   v_plus [3x1]        velocity at infinitive in the outcoming 
%                                       leg of the hyperbola [L/T]
%                   a_minus [1]         semimajor axis of the incoming leg
%                                       of the hyperbola [L]
%                   a_plus [1]          semimajor axis of the outcoming leg
%                                       of the hyperbola [L]
%                   e_minus [1]         eccentricity of the incoming leg
%                                       of the hyperbola [-]
%                   e_plus [1]          eccentricity of the outcoming leg
%                                       of the hyperbola [-]
%                   rp [1]              magnitude of the radius at perigee
%                                       (when deltaV is performed) [L]
%
% OUTPUTS
%   struct      containing the orbital parameters of the incoming and
%               outcoming legs of the hyperbolic trajectory
%
% VERSIONS
% 04/12/2019 First version
% 30/01/2020 Second version
%
% Functions needed
%   astroConstants
%   Keplerian2CartesianOrbit
%   Kepler
%   TimeLawHyp
%
% Images needed
%   Earth.jpg

vinf_minus = struct.v_minus;
vinf_plus = struct.v_plus;
a_minus = struct.a_minus;
struct.entry_leg.semimajor_axis = a_minus;
a_plus = struct.a_plus;
struct.exit_leg.semimajor_axis = a_plus;
% e_minus = struct.e_minus;
% struct.entry_leg.eccentricity = e_minus;
% e_plus = struct.e_plus;
% struct.exit_leg.eccentricity = e_plus;
rp = struct.r_p;

e_x = [1;0;0];
e_z = [0;0;1];
k = astroConstants(13);

vp_minus = sqrt(k*(2/rp-1/a_minus));

% computation of the normal vector and of the inclination of the orbital
% plane
h_vector = cross(vinf_minus,vinf_plus)/norm(cross(vinf_minus,vinf_plus));
% incoming leg
h_mag_minus = rp*vp_minus;
h_minus = h_mag_minus*h_vector;

i = acos(dot(h_vector,e_z));
struct.entry_leg.inclination = i;
struct.exit_leg.inclination = i;

% computation of the right ascension of the ascending node of the orbital
% plane
N = cross(e_z,h_vector);
N_mag = norm(N);

if N(2)>0
    omega = acos(dot(e_x,N)/N_mag);
elseif N(2)<0
    omega = 2*pi-acos(dot(e_x,N)/N_mag);
elseif i == 0 || i == pi %singularity if the orbit is not inclined
    omega = 0;
    N = e_x;
    N_mag = norm(N);
end
struct.entry_leg.RAAN = omega;
struct.exit_leg.RAAN = omega;

% computation of the argument of perigee of the incoming leg of the
% hyperbola exploiting the eccentricity vector of this leg
ecc_minus = cross(vinf_minus,h_minus)/k+vinf_minus/norm(vinf_minus);
e_minus = norm(ecc_minus);
struct.entry_leg.eccentricity = e_minus;
struct.entry_leg.e_vector = ecc_minus;

if ecc_minus(3)>=0
    omeghino_minus = acos(dot(N,ecc_minus)/(N_mag*norm(ecc_minus)));
elseif ecc_minus(3)<0
    omeghino_minus = 2*pi-acos(dot(N,ecc_minus)/(N_mag*norm(ecc_minus)));
elseif e_minus == 0 %singularity if orbit is circular
    omeghino_minus = 0;
end
struct.entry_leg.argument_of_perigee = omeghino_minus;

% computation of the true anomaly of the entry inside the sphere of
% influence of Earth
r_SOI = 145.3*astroConstants(23);
p_minus = abs(a_minus*(e_minus.^2-1));
struct.entry_leg.true_anomaly = -acos((1/e_minus)*((p_minus/r_SOI)-1));
[r0,v0] = Keplerian2CartesianOrbit(struct.entry_leg,k);

% outcoming leg 
vp_plus = sqrt(k*(2/rp-1/a_plus));
h_mag_plus = rp*vp_plus;
h_plus = h_mag_plus*h_vector;

% computation of the argument of perigee of the outcoming leg of the
% hyperbola exploiting the eccentricity vector of this leg
ecc_plus = cross(vinf_plus,h_plus)/k-vinf_plus/norm(vinf_plus);
e_plus = norm(ecc_plus);
struct.exit_leg.eccentricity = e_plus;
struct.exit_leg.e_vector = ecc_plus;

if ecc_plus(3)>=0
    omeghino_plus = acos(dot(N,ecc_plus)/(N_mag*norm(ecc_plus)));
elseif ecc_plus(3)<0
    omeghino_plus = 2*pi-acos(dot(N,ecc_plus)/(N_mag*norm(ecc_plus)));
elseif ecc_plus == 0 %singularity if orbit is circular
    omeghino_plus = 0;
end
struct.exit_leg.argument_of_perigee = omeghino_plus;

% computation of the true anomaly of the exit from the sphere of influence of Earth
p_plus = abs(a_plus.*(e_plus.^2-1));
struct.exit_leg.true_anomaly = acos((1./e_plus)*((p_plus./r_SOI)-1)); 
[r1,v1] = Keplerian2CartesianOrbit(struct.exit_leg,k);

% integration of the equations of motion for both hyperbolas
[struct.time_of_flight_minus] = TimeLawHyp (r_SOI,e_minus,-a_minus);
[struct.time_of_flight_plus] = TimeLawHyp (r_SOI,e_plus,-a_plus);

[~,rr] = Kepler(k,[0 struct.time_of_flight_minus+3],[r0;v0]);
[~,rrr] = Kepler(k,[0 -struct.time_of_flight_plus-3],[r1,v1]);

r_entry = rr(:,1:3)./ astroConstants(23);
r_exit = rrr(:,1:3)./ astroConstants(23);

% plot
figure;
hold on;
plot3(r_entry(:,1),r_entry(:,2),r_entry(:,3));
grid on;
plot3(r_exit(:,1),r_exit(:,2),r_exit(:,3));
axis equal;
hold on
[X,Y,Z] = ellipsoid(0,0,0,6378,6378,6357);
s = surf(X/astroConstants(23),Y/astroConstants(23),-Z/astroConstants(23),'FaceColor','None', 'edgecolor', 'none');
image = imread('Earth','jpg');
set(s,'FaceColor','texturemap','CData',image);
xlabel('rx [Earth''s radii]','Color','k')
ylabel('ry [Earth''s radii]','Color','k')
zlabel('rz [Earth''s radii]','Color','k')
legend('Entry leg','Exit leg');

title('Fly-by hyperbolic trajectory')
