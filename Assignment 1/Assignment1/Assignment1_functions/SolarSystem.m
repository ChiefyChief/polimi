function SolarSystem (planets,date)
% Plot of the Solar System planets at a given date. In this plot, we
% are considering just Jupiter, Earth and Venus, which are the departing
% planet, the gravity assist planet and the arriving one
%
% PROTOTYPE
%   SolarSystem (planets,date)
%
% INPUTS
%   planets     struct containing the characteristic of all the planets
%               involved
%   date        struct with the dates relatively to the manoeuvre
% 
% OUTPUTS
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
%
% Functions needed
%   PlotOrbitPlanet
%   PlotPlanet
%   InitialisingPlanets

% Sun = InitialisingPlanets(date,10);

figure;
hold on
[Sun] = InitialisingPlanets(date,10);
PlotPlanet(Sun)
[R_0_1] = PlotOrbitPlanet(planets.departure,date);
%PlotPlanet (planets.departure,R_0_1)
[R_0_2] = PlotOrbitPlanet(planets.flyby,date);
%PlotPlanet (planets.flyby,R_0_2)
[R_0_3] = PlotOrbitPlanet(planets.arrival, date);
PlotPlanet (planets.departure,R_0_1)
PlotPlanet (planets.flyby,R_0_2)
PlotPlanet (planets.arrival,R_0_3)
grid off

legend('Sun','Jupiter','Earth','Venus')
xlabel('rx [AU]','Color','k')
ylabel('ry [AU]','Color','k')
zlabel('rz [AU]','Color','k')
return