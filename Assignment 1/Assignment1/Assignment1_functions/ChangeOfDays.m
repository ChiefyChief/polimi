function [window_date] = ChangeOfDays (t0,tf)
% Modifies the day of the date in a given matrix between the two times
% provided
%
% INPUTS
%   t0 The initial time 
%   tf The final time
% OUTPUTS
%   window_date The matrix contains the rows of dates 
% VERSIONS
%   28/11/2019  First version


% The function compose a matrix in which each row refers to a date. In
% particular the first row is the initial given date, and from row to the
% other the months and the days are gradually incremented.

% Creation of the window matrix
count_day = 0;
% Check of the leap years
[bis, bis_years] = LeapYearCheck(t0,tf);
% Composition of the matrix considering only the change of months between
% the initial and final given dates
window_month = ChangeOfMonths (t0,tf);

% Evaluation of the total number of days between the two given dates
% starting from the analysis of the months considered in the window.
% Initial month and final one are not considered and added after the cycle.
for i = 2:length(window_month)-1
    count_day = count_day + DaysInAMonth(window_month(i,2));
    if window_month(i,2) == 02
        % Check of the leap year
        for yr = 1:length(bis_years)
            if window_month(i,1) == bis_years(yr)
                % We are adding the 29
                count_day = count_day + 1;
            end
        end
    end
end

% Initial date: we add the initial month's days
% in_bis = 0 even if we are in february we have 28 days beacuse we aren't
% in a leap year;
% in_bis = 1 initial date is in february during a leap year 
in_bis = 0;
if bis(1) && t0(1) == bis_years(1)
    count_day = count_day + (29-t0(3));
    in_bis = 1;
else
    count_day = count_day + (DaysInAMonth(t0(2))-t0(3));
end

% Final date: we add the final month's days
% fin_bis = 0 even if we are in february we have 28 days beacuse we aren't
% in a leap year;
% fin_bis = 1 initial date is in february during a leap year 
fin_bis = 0;
if bis(2) && tf(1) == bis_years(end)
    count_day = count_day + (tf(3))+1;
    fin_bis = 1;
else
    count_day = count_day + tf(3);
end

% Creation of the final matrix of the days length
window_date = zeros(count_day,3);
% Inserting the initial and final dates
window_date(1:end-1,1) = t0(1);
window_date(end,1) = tf(1);
window_date(1:end-1,2) = t0(2);
window_date(1,3) = t0(3);
window_date(end,2) = tf(2);
window_date(end,3) = tf(3);

% Fulfillment of the days in the first month
n_day = DaysInAMonth(t0(2));
if in_bis
    n_day = n_day + 1;
end
% position pointer, it is incremented during the while cicle till it
% reaches the value of the total days within the window.
i = 2;
while (window_date(i-1,3)<=n_day)
    window_date(i,3) = window_date(i-1,3)+1;
    i = i+1;
end
window_date(i-1:end-1,2) = t0(2)+1;
window_date(i-1,3) = 1;

% Fulfillment of the overall matrix with the same process used for the
% first month.
for mt = 2:length(window_month)
    n_day = DaysInAMonth(window_month(mt,2));
    % Leap year check
    if window_month(mt,2) == 2
        for yr = 1:length(bis_years)
            if window_month(mt,1) == bis_years(yr)
                % we are adding the 29
                n_day = n_day + 1; 
            end
        end
    end
        % While cycle controls both the pointer i to be minor than the
        % number of days within the window and the number of day we reached
        % by incrementing of one the day date during the cycle.
        while (window_date(i-1,3)<=n_day) && (i<=count_day)
            window_date(i,3) = window_date(i-1,3) + 1;
            i = i+1;
        end
        % Case in which we exit from the cycle becuase we have finished to
        % fulfill the days of the month considered for a certain length of
        % the window.
        if (i<=count_day)
        window_date(i-1,3) = 1;
        window_date(i-1:end,2) = window_date (i-1,2)+1;
        % Check on the number of month we are, if 12 (December) passed we
        % impose the month equal to 1 (Jenaury) and increment the year.
        if window_date(i-1,2) == 13
            window_date(i-1:end,1) = window_date(i-1,1)+1;
            window_date(i-1:end,2) = 1;
        end
        end
end