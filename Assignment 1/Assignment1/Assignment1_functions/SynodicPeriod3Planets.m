function [T] = SynodicPeriod3Planets(date,planet1,planet2,planet3)
% Function that evaluate the SYnodic period between three planets in
% defined range if time.

% PROTOTYPE
%   [date_synodic_period, synodic_period] = SynodicPeriod3Planets(complete_time_window,5,3,2)
%
% INPUTS
%   date[nx6]       matrix of the time window
%   planet1 [1]     id reference number of Jupiter
%   planet2 [1]     id reference number of Earth
%   planet3 [1]     id reference number of Venus
%
% OUTPUTS
%   T[2x6]          time vector [T]
%   synodic_period[1]            synodic period
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian
%
% Functions needed
% InitialisingPlanets
% WindowCreation
%
% we do it from raw data

if 0
    planet_1 = InitialisingPlanets(date(1,:),planet1);
    planet_2 = InitialisingPlanets(date(1,:),planet2);
    planet_3 = InitialisingPlanets(date(1,:),planet3);

    period_1 = planet_1.period;
    period_2 = planet_2.period;
    period_3 = planet_3.period;

    synodic_period_1 = 1 / abs(1 / period_1 - 1 / period_2);
    synodic_period_2 = 1 / abs(1 / period_2 - 1 / period_3);

    c = lcm(floor(synodic_period_1),floor(synodic_period_2));
    T = c/(3600*24*365);
end

T = zeros(2,6);

if 1
    T = [];
    toll = 0.016;
    
    for i = 1:size(date,1)
        % Planet values
        planet_1 = InitialisingPlanets(date(i,:),planet1);
        planet_2 = InitialisingPlanets(date(i,:),planet2);
        planet_3 = InitialisingPlanets(date(i,:),planet3);
    
        teta_1 = planet_1.true_anomaly;
        teta_2 = planet_2.true_anomaly;
        teta_3 = planet_3.true_anomaly;
        
        toll_1 = toll > abs(teta_1-teta_2);
        toll_2 = toll > abs(teta_2-teta_3);
        toll_3 = toll > abs(teta_1-teta_3);
        
        if toll_1 && toll_2 && toll_3
            % If the tolerance condition is respected the planets are 
            % considered to be aligned, therefore the time in which this 
            % happens is saved 
            T = [T; date(i,:)];
        end
    end
end

return