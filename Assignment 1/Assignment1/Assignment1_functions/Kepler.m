function [t,y] = Kepler(k,T,y0)
% Function to solve the equation of motion of the 2BP
%
% PROTOTYPE
%   [t,y] = Kepler(k,T,y0)
%
% INPUTS
%   k[1]            gravitational parameter of the main attractor [L^3/T^2]
%   T[2x1]          time vector (initial and final times) [T]
%   y0[6x1]         initial conditions vector [L;L/T]
%
% OUTPUTS
%   t[nx1]          time vector [T]
%   y[nx6]          position and velocity vector [L;L/T]
%
% VERSIONS
% 28/11/2019    First version
%
% Functions needed
%

tspan=T;
%odefun
odefun = @(t,y) [y(4);
    y(5);
    y(6);
    -k.*y(1)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -k.*y(2)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -k.*y(3)./(y(1).^2+y(2).^2+y(3).^2)^1.5];
options = odeset('RelTol',1e-13,'AbsTol',1e-14);

%result
[t,y] = ode113(odefun,tspan,y0,options);