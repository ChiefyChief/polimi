function [orbit] = InitialisingPlanets(time, planet)
% Function to put all of the orbital parameters for a given planet at a
% given time into one struct
%
% PROTOTYPE
%   [orbit] = InitialisingPlanets(time, planet)
%
% INPUTS
%   time            The time at which the orbit parameters refer to
%   planet          The numeric value of the planet
%
% OUTPUTS
%   orbit          The structure containing all of the orbital parameters
%                  for the orbit
%
% VERSIONS
% 28/11/2019    First version
%
% Functions needed
%

switch planet
    case 1
        orbit.name = 'Mercury';
        orbit.id_code = 1;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 1.5*2439.7; %km
        orbit.image = 'Mercury.jpg';
        orbit.gravitational_constant = 2.2032e+04;
    case 2
        orbit.name = 'Venus';
        orbit.id_code = 2;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 1.5*6051.8; %km
        orbit.image = 'Venus.jpg';
        orbit.gravitational_constant = 3.2486e+05;
    case 3
        orbit.name = 'Earth';
        orbit.id_code = 3;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 1.5*6371; %km
        orbit.image = 'Earth.jpg';
        orbit.gravitational_constant = 3.9860e+05;
    case 4
        orbit.name = 'Mars';
        orbit.id_code = 4;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 1.5*3389.5; %km
        orbit.image = 'Mars.jpg';
        orbit.gravitational_constant = 4.2828e+04;
    case 5
        orbit.name = 'Jupiter';
        orbit.id_code = 5;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 0.5*69911; %km 
        orbit.image = 'Jupiter.jpg';
        orbit.gravitational_constant = 1.2671e+08;
    case 6
        orbit.name = 'Saturn';
        orbit.id_code = 6;
        orbit.radius = 58232; %km
        orbit.image = 'Saturn.jpg';
        orbit.gravitational_constant = 3.7941e+07;
    case 7
        orbit.name = 'Uranus';
        orbit.id_code = 7;
        orbit.radius = 25362; %km
        orbit.image = 'Uranus.jpg';
        orbit.gravitational_constant = 5.7945e+06;
    case 8
        orbit.name = 'Neptune';
        orbit.id_code = 8;
        orbit.radius = 24622; %km
        orbit.image = 'Neptune.jpg';
        orbit.gravitational_constant = 6.8365e+06;
    case 9
        orbit.name = 'Pluto';
        orbit.id_code = 9;
        orbit.radius = 1188.3; %km
        orbit.image = 'Pluto.jpg';
        orbit.gravitational_constant = 981.6010;
    case 10
        orbit.name = 'Sun';
        orbit.id_code = 10;
        %{
            WARNING: This has been alatered for figure purporses.
        %}
        orbit.radius = 0.03 * 695510; %km
        orbit.image = 'Sun.jpg';
        orbit.gravitational_constant = 4.9028e+03;
end

% Evaluating keplerian parameters with uplanet in a given window time
time_s = date2mjd2000(time);
[kep,mu_sun] = uplanet(time_s,planet);

% Giving names to the parameters inside a struct
orbit.semimajor_axis = kep(:,1);
orbit.eccentricity = kep(:,2);
orbit.inclination = kep(:,3);
orbit.RAAN = kep(:,4);
orbit.argument_of_perigee = kep(:,5);
orbit.true_anomaly = kep(:,6);
orbit.period = 2 * pi * sqrt(orbit.semimajor_axis^3 / mu_sun);

orbit.mu_sun = mu_sun;