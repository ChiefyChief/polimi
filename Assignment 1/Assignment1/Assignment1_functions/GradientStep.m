function [converged, next_step_index, minimum_delta_v, time_of_flight, crashed, minimum_power_ga] = GradientStep(stencil)
% Calculates the delta v requirements in all of the directions and compares
% them to the delta v requirement for the central node. 
% PROTOTYPE
%   [converged, next_step_index, minimum_delta_v, time_of_flight, crashed, minimum_power_ga] = GradientStep(stencil)
%
% INPUTS
%   stencil The structure containing the 3D space infromation to calcualte
%   all the delta v expendatures
% 
% OUTPUTS
%   converged Boolean indicator if the optimiser has converged
%   next_step_index The next step to to act as the central node
%   minimum_delta_v The minimum delta v found in this step
%   time_of_flight The time of flight of this step
%   crashed Boolean indicator if the spacecraft crashed
%   minimum_power_ga The structure containing everything regarding PGA
%
% VERSIONS
%   28/11/2019  First version

    converged = false;
    
    [ central.delta_v, central.time_of_flight, crashed, central.power_ga ] = InterplanetaryTransferWithGravityAssist(stencil.central.orbit, stencil.central.date);

    node = stencil.nodes;
    
        for i = 1 : 6
            [node(i).delta_v, node(i).time_of_flight, crashed, node(i).power_ga ] = InterplanetaryTransferWithGravityAssist(node(i).orbit, node(i).date);
        end
   
    
    minimum_delta_v = central.delta_v;
    time_of_flight = central.time_of_flight;
    minimum_power_ga = central.power_ga;
    
    found_smaller_value = false;

        for i = 1 : 6
            if (minimum_delta_v > node(i).delta_v)
            
                minimum_delta_v = node(i).delta_v;
                time_of_flight = node(i).time_of_flight;
                next_step_index = stencil.nodes(i).index;
                minimum_power_ga = node(i).power_ga;

                found_smaller_value = true;
            end
        end

        if ~found_smaller_value
            converged = true;
            next_step_index = stencil.central.index;
        end
end