function PlotPlanet (orbit,R_0)

R = orbit.radius;
AU = astroConstants(2);

if nargin == 1
    R_0 = [0 0 0];
    [X,Y,Z] = sphere;
    s = surf(X.*(R/AU)+R_0(1),Y.*(R/AU)+R_0(2),-Z.*(R/AU)+R_0(3),'FaceColor','None', 'edgecolor', 'none');
    image = imread(orbit.image);
    set(s,'FaceColor','texturemap','CData',image);
else
    [X,Y,Z] = sphere;
    s = surf(1000*X.*(R/AU)+R_0(1),1000*Y.*(R/AU)+R_0(2),-1000*Z.*(R/AU)+R_0(3),'FaceColor','None', 'edgecolor', 'none');
    image = imread(orbit.image);
    set(s,'FaceColor','texturemap','CData',image);
end

s.Annotation.LegendInformation.IconDisplayStyle = 'off';

end
