function [POWER_GA] = PowerGA (V_minus, V_plus, orbit, h_atm)  
 
%INPUT 
%V_minus = velocity of the s/c on the transfer arc calcuate thanks to the 
%          Lambert problem from Jupiter to Earth [Km/s] 
%V_plus = velocity of the s/c on the trasfer arc calculate thanks to the 
%           Lambert problem from Earth to Venus [Km/s] 
%flyby_date = date of flyby from the optimisation process  
%h_atm = altitude of the planet atmosphere we want to make the fly by 
%        around [Km] 
%R_P = radius of the planet we want to make the fly by  
 
%OUTPUT  
%POWER_GA = structure with fields
            %V_minus = incoming heliocentric velocity of the s/c (vector) [Km/s] 
            %V_plus = outcoming heliocentric velocity of the s/c (vector) [Km/s]
            %v_minus = incoming relative velocity of the s/c wrt the planet 
            %          (vector) [Km/s]
            %v_plus = outcoming relative velocity of the s/c wrt the planet 
            %         (vector) [Km/s] 
            %r_p = radius of perigee of the hyoerbola [Km] 
            %e_minus = incoming leg eccentricity [-] 
            %h_minus = incoming leg angular momentum [Km^2/s] 
            %p_minus = incoming leg semi latus rectum [Km] 
            %a_minus = incoming leg semi major axis [Km]  
            %e_plus = outcoming leg eccentricity [-] 
            %h_plus = outcoming leg angular momentum [Km^2/s] 
            %p_plus = outcoming leg semi latus rectum [Km] 
            %a_plus = outcoming leg semi major axis [Km] 
            %DeltaV_p = change in velocity due to the maneouvre [Km/s] 
            %DeltaV_inf = change in velocity due to the planet only [Km/s]
            %Time_of_flight_entry_leg = time of flight in the incoming leg
            %of the hyperbolic gravity assist [s]
            %Time_of_flight_exit_leg = time of flight in the outcoming leg
            %of the hyperbolic gravity assist [s]
            %Total_time_of_flight = time of flight of the entire hyperbolic
            %trajectory [s]
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

%Velocity of the Earth, considering it on its orbit during the flyby time 

mu_sun = astroConstants(4);

[~,V_P] = Keplerian2CartesianOrbit(orbit, mu_sun); 
 
mu_planet = orbit.gravitational_constant; 
r_planet = orbit.radius; 
 
rp_lim = r_planet + h_atm; 
 
%Relative velocity of the s/c wrt the Earth
POWER_GA.V_minus = V_minus;
POWER_GA.V_plus = V_plus;
POWER_GA.v_minus = V_minus - V_P; 
POWER_GA.v_plus = V_plus - V_P; 
 
delta = acos(dot(POWER_GA.v_minus,POWER_GA.v_plus) / (norm(POWER_GA.v_minus) * norm(POWER_GA.v_plus)) ); 
 
v_minus_norm = norm(POWER_GA.v_minus); 
v_plus_norm = norm(POWER_GA.v_plus); 
 
%The radius of the periapsis of the hyperbola  
fun = @(x) delta - (asin(1/(1+((x*v_minus_norm^2)/mu_planet))) + asin(1/(1+((x*v_plus_norm^2)/mu_planet)))); 
 
OPTIONS = optimset ('TolX',1e-13,'Display','off'); 
%Initial guess : the radius of the periapsis of the hyperbola equal to the 
%radius of the planet, which means that we pass on the surface of the 
%planet 
x0 = r_planet;  
[x] = fsolve (fun,x0,OPTIONS); 
 
POWER_GA.r_p = norm(x); 
    
    %Hyperbola branches characteristics  
    POWER_GA.e_minus = 1+((POWER_GA.r_p*v_minus_norm^2)/mu_planet); 
    POWER_GA.h_minus = sqrt(POWER_GA.r_p*mu_planet*(1+POWER_GA.e_minus)); 
    POWER_GA.p_minus = POWER_GA.h_minus^2/mu_planet; 
    POWER_GA.a_minus = POWER_GA.p_minus/(1-POWER_GA.e_minus^2); 
 
    POWER_GA.e_plus = 1+((POWER_GA.r_p*v_plus_norm^2)/mu_planet); 
    POWER_GA.h_plus = sqrt(POWER_GA.r_p*mu_planet*(1+POWER_GA.e_plus)); 
    POWER_GA.p_plus = POWER_GA.h_plus^2/mu_planet; 
    POWER_GA.a_plus = POWER_GA.p_plus/(1-POWER_GA.e_plus^2); 
 
    %velocity at the periapsis of the hyperbola  
    v_p_minus = sqrt(mu_planet/POWER_GA.p_minus)*(1+POWER_GA.e_minus); 
    v_p_plus = sqrt(mu_planet/POWER_GA.p_plus)*(1+POWER_GA.e_plus); 
 
    POWER_GA.DeltaV_p = abs(v_p_minus-v_p_plus); 
    POWER_GA.DeltaV_inf = norm(POWER_GA.v_plus - POWER_GA.v_minus);
    
    %time of flight on the hyperbola
    r_SOI = 145.3*astroConstants(23);
    [POWER_GA.Time_of_flight_entry_leg] = TimeLawHyp (r_SOI,POWER_GA.e_minus,-POWER_GA.a_minus);
    [POWER_GA.Time_of_flight_exit_leg] = TimeLawHyp (r_SOI,POWER_GA.e_plus,-POWER_GA.a_plus);
    POWER_GA.Total_time_of_flight = POWER_GA.Time_of_flight_entry_leg+POWER_GA.Time_of_flight_exit_leg;
    
if x > rp_lim     
    POWER_GA.crashed = false;
else 
    POWER_GA.crashed = true;
end 
 
end