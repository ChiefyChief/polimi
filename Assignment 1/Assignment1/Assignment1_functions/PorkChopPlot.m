function PorkChopPlot(window_departure_dates, window_arrival_dates, delta_v, time_of_flight) 
% Plots a pork chop plot with given delta v data and time windows for departure and arrival
% This function will first convert the matrix of the window departure and arrival
% dates into an array of corresponding strings to be used within the graph. 
% Then this will simply plot the contour with a maxmimum delta of XX km/s and 
% the on top of this the time of flight contour plot will be done in units of days.
%
% PROTOTYPE
%   PorkChopPlot(window_departure_dates, window_arrival_dates, delta_v, time_of_flight)
%
% INPUTS
%   window_departure_dates A matrix of the time window of deperature dates in the format [ YYYY MM DD HH MM SS ]
%   window_arrival_dates A matrix of the time window of the arrival dates in the format [ YYYY MM DD HH MM SS ]
%   delta_v A matrix of delta v corresponding to the departure and arrival dates in units of [ km / s]
%   time_of_flight A matrix consisting of the time of flight of the transfer in units of [s]
% OUTPUTS
%   output A structure containing information about the adjacent nodes
% VERSIONS
%   28/11/2019  First version


    %{
        Just generating the strings to be used as the x tick labels on the pork chop plot,
        the data input is in the format of [ YYYY MM DD HH MM SS ] are we convert it to
        [ DD MM YYYY ] as the hours, minutes, and seconds are not as critical

        Departure date conversion
    %}
    for i = 1 : length(window_departure_dates(:,1))
        departure_date(i) = ...
            num2str(window_departure_dates(i,3)) + " " + ...
            num2str(window_departure_dates(i,2)) + " "+ ...
            num2str(window_departure_dates(i,1));
    end

    % Arrival date conversion
    for i = 1 : length(window_arrival_dates(:,1))
        arrival_date(i) = ...
            num2str(window_arrival_dates(i,3)) + " " + ...
            num2str(window_arrival_dates(i,2)) + " "+ ...
            num2str(window_arrival_dates(i,1));
    end

    %{
        Setting the font for the graphs to reduce a job during post-processing. As the report
        is going to be made in Latex the font should match to ensure consistency between graphs 
        and report
    %}
    set(0,'DefaultTextFontname', 'CMU Serif');
    set(0,'DefaultAxesFontName', 'CMU Serif');
    set(0,'defaulttextinterpreter','latex');

    figure;

    contour_interval = round((ceil(max(max(delta_v))) - floor(min(min(delta_v)))) / 5);
    % Simple contour plot with steps between contour lines being in intervals of 1 between the values of 0 and 10 
    [delta_v_matrix, delta_v_object] = contour(delta_v, floor(min(min(delta_v))):contour_interval:ceil(max(max(delta_v))));
    clabel(delta_v_matrix, delta_v_object, 'FontSize',10,'FontName','CMU Serif'); % TODO - Check if needed since font is set above
    delta_v_object.LineWidth = 1;

    xlabel('Departure Date [DD MM YYYY]', 'interpreter', 'latex');
    xticklabels(departure_date);
    xtickangle(45);
    ylabel('Arrival Date [DD MM YYYY]', 'interpreter', 'latex');
    yticklabels(arrival_date);

    % Show the colour bar to improve ease of reading of the graph
    colorbar_object = colorbar;
    colorTitleHandle = get(colorbar_object,'Title');
    titleString = '\DeltaV [km/s]';
    set(colorTitleHandle ,'String',titleString);
    caxis([floor(min(min(delta_v))) ceil(max(max(delta_v)))]);

    grid on;
    hold on;

    time_of_flight = time_of_flight ./ (24*3600); % Conversion to days from seconds
    
    interval = round((ceil(max(max(time_of_flight))) - floor(min(min(time_of_flight)))) / 5);
    
    [C_tof,h_tof] = contour(time_of_flight,min(min(time_of_flight)):interval:max(max(time_of_flight)),'k--');
    clabel(C_tof,h_tof,'FontSize',10,'FontName','CMU Serif'); % TODO - Check if needed since font is set above
    h_tof.LineWidth = 1;

    hold off;
end