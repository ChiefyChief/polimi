function [delta_v, time_of_flight, crashed, power_ga ] = InterplanetaryTransferWithGravityAssist(orbit, date)

time_of_flight_departure_to_flyby = 24 * 3600 * (date2mjd2000(date.flyby) - date2mjd2000(date.departure));
time_of_flight_flyby_to_arrival = 24 * 3600 * (date2mjd2000(date.arrival) - date2mjd2000(date.flyby));
mu_sun = astroConstants(4);

[velocity_departure_to_flyby] = Lambert(orbit.departure, orbit.flyby, time_of_flight_departure_to_flyby, mu_sun);
[velocity_flyby_to_arrival] = Lambert(orbit.flyby, orbit.arrival, time_of_flight_flyby_to_arrival, mu_sun);

[power_ga] = PowerGA(velocity_departure_to_flyby.arrival_raw, velocity_flyby_to_arrival.departure_raw, orbit.flyby, 150); 

crashed = power_ga.crashed;

delta_v = norm(velocity_departure_to_flyby.departure) + power_ga.DeltaV_p + norm(velocity_flyby_to_arrival.arrival);
    
time_of_flight = (time_of_flight_departure_to_flyby + time_of_flight_flyby_to_arrival) / 24 / 3600;

end