function [R_0] = PlotOrbitPlanet(planet, starting_date, end_date)
% Plot of planet's orbit given an initial date or given an initial and final dates
% 
% PROTOTYPE
% PlotOrbitPlanet(orbit, starting_date, end_date)
%
% INPUT
%   planet [struct]     orbit characteristic we want to deal with
%   starting_date[1x6]  initial date[year,month,day,hour,minute,second]
%   end_date[1x6]       final date (if we need to plot just a portion of the orbit
%                       otherwise entire orbit will be plotted[year,month,day,hour,minute,second]
%
% OUTPUT
%   graph of the planet
%
% VERSION
% 28/11/2019    First version
%
% Functions needed
%   time conversion
%   uplanet and ephNEO, Planet and NEO
%   astroConstants
%   WindowCreation
%   Keplerian2CartesianOrbit

starting_date_mjd2000 = date2mjd2000(starting_date);
planet_id = planet.id_code;

% we find the ephemerides of the planet in the initial date
if planet_id < 11
    [orbit, mu_sun] = uplanet(starting_date_mjd2000, planet_id);
else
    [orbit] = ephNEO(starting_date_mjd2000, planet_id);
    mu_sun = astroConstants(4);
end

% If we have just the initial date, we are going to plot an entire
% revolution of the planet.

% Computation of the semi-major axis in order to find the period of
% revolution.
a = orbit(1);
T = 2 * pi * sqrt(a.^3 / mu_sun);
T_days = T/(3600*24);

% Initializing the time window.
[time_window] = WindowCreation([2020 1 1],[2099 1 1]);

% Setting the days' vector.
period_days = [];
for i = 1:size(time_window,1)
    if ismember(starting_date,time_window(i,:),'rows') == 1
        period_days = time_window((i:(i+round(T_days))),:);
    end
end

% In case we have also the final date.
if nargin == 3
    for k = 1:size(period_days,1)
        if ismember(end_date,period_days(k,:),'rows') == 1
            period_days = period_days(1:k,:);
            break
        end
    end
end

% Computing the position of the planet for every day.
r = zeros(3,length(period_days));
for j = 1:size(period_days,1)
    time = date2mjd2000(period_days(j,:));
    if planet_id < 11
        [orbit1,mu_sun] = Planet(time,planet_id);
    else
        [orbit1] = NEO(time,planet_id);
    end
    [r(:,j),~] = Keplerian2CartesianOrbit(orbit1, mu_sun);
end  


% Plot
AU = astroConstants(2);

%Position of the planet at departure date
R_0 = [r(1,1)./AU, r(2,1)./AU, r(3,1)./AU];
PlotPlanet(planet,R_0);
    
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');


hold on
if isfield(planet, 'line_style') && isfield(planet, 'line_colour')
    h = plot3(r(1,:)./AU,r(2,:)./AU,r(3,:)./AU, planet.line_style, 'Color', planet.line_colour,'LineWidth', 0.6, 'MarkerSize', 3);
else
    h = plot3(r(1,:)./AU,r(2,:)./AU,r(3,:)./AU, '--','LineWidth',1);
end

if isfield(planet, 'legend')
    if ~planet.legend
        h.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end
end

%PlotPlanet( planet, R_0)
grid on
axis equal
xlabel('rx [AU]','Color','k')
ylabel('ry [AU]','Color','k')
zlabel('rz [AU]','Color','k')
view([1 1 1])