function [orbit, mu_sun] = Planet(mjd2000, planet_id)
% Output of uplanet assigned in struct form

    [keplerian_array, mu_sun] = uplanet(mjd2000, planet_id);

    orbit.semimajor_axis = keplerian_array(1);
    orbit.eccentricity = keplerian_array(2);
    orbit.inclination = keplerian_array(3);
    orbit.RAAN = keplerian_array(4);
    orbit.argument_of_perigee = keplerian_array(5);
    orbit.true_anomaly = keplerian_array(6);
end