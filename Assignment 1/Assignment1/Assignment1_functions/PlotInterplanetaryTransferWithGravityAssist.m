function PlotInterplanetaryTransferWithGravityAssist(dates, planets, file_string)

departure_date = dates.departure;
flyby_date = dates.flyby;
arrival_date = dates.arrival;

departure_planet = planets.departure;
flyby_planet = planets.flyby;
arrival_planet = planets.arrival;

time_departure_seconds = date2mjd2000(departure_date);
time_flyby_seconds = date2mjd2000(flyby_date);
time_arrival_seconds = date2mjd2000(arrival_date);

time_of_flight_departure_to_flyby = 24*3600*(time_flyby_seconds - time_departure_seconds);
time_of_flight_flyby_to_arrival = 24*3600*(time_arrival_seconds - time_flyby_seconds);

AU = astroConstants(2);

% computation of the orbital parameters and then of the cartesian
% coordinates of the departing and arriving points
if departure_planet.id_code < 11
    [orbit_departure, mu_sun] = Planet(time_departure_seconds, departure_planet.id_code);
else
    [orbit_departure] = NEO(time_departure_seconds, departure_planet.id_code);
    mu_sun = astroConstants(4);
end
[R_departure] = Keplerian2CartesianOrbit(orbit_departure,mu_sun);


if flyby_planet.id_code < 11
    [orbit_flyby, mu_sun] = Planet(time_flyby_seconds, flyby_planet.id_code);
else
    [orbit_flyby] = NEO(time_flyby_seconds, flyby_planet.id_code);
    mu_sun = astroConstants(4);
end
[R_flyby] = Keplerian2CartesianOrbit(orbit_flyby,mu_sun);


if arrival_planet.id_code < 11
    [orbit_arrival,mu_sun] = Planet(time_arrival_seconds, arrival_planet.id_code);
else
    [orbit_arrival] = NEO(time_arrival_seconds, arrival_planet.id_code);
    mu_sun = astroConstants(4);
end
[R_arrival] = Keplerian2CartesianOrbit(orbit_arrival,mu_sun);


% computation of the transfer orbit with lambertMR
[~,~,~,~,VI_departure_to_flyby,~,~,~] = lambertMR(R_departure(:,1), R_flyby(:,end), time_of_flight_departure_to_flyby, mu_sun, 0, 0, 0, 2);
[~,~,~,~,VI_flyby_to_arrival,~,~,~] = lambertMR(R_flyby(:,1), R_arrival(:,end), time_of_flight_flyby_to_arrival, mu_sun, 0, 0, 0, 2);

% integration of the equation of motion for the transfer orbit given the
% initial position and velocity
X0 = [R_departure(:,1); VI_departure_to_flyby'];
[~,X_departure_to_flyby] = Kepler(mu_sun,[0 time_of_flight_departure_to_flyby],X0);

X0 = [R_flyby(:,1); VI_flyby_to_arrival'];
[~,X_flyby_to_arrival] = Kepler(mu_sun,[0 time_of_flight_flyby_to_arrival],X0);

figure;
% plot
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');

set(gcf, 'Position',  [0, 0, 1000, 600]);

X_departure_to_flyby = X_departure_to_flyby ./ AU;
X_flyby_to_arrival = X_flyby_to_arrival ./ AU;

hold on
grid on

plot3(X_departure_to_flyby(:,1),X_departure_to_flyby(:,2),X_departure_to_flyby(:,3),'.-','Color', [0, 0.4470, 0.7410],'LineWidth',0.8);
plot3(X_flyby_to_arrival(:,1),X_flyby_to_arrival(:,2),X_flyby_to_arrival(:,3),'.-','Color', [0.8500, 0.3250, 0.0980],'LineWidth',0.8);

planets.departure.line_colour = [0.4660, 0.6740, 0.1880];
planets.departure.line_style = '.-';
PlotOrbitPlanet(planets.departure,arrival_date);

planets.flyby.line_colour = [0.4940, 0.1840, 0.5560];
planets.flyby.line_style = '.-';
PlotOrbitPlanet(planets.flyby,arrival_date);

planets.arrival.line_style = '.-';
planets.arrival.line_colour = [0.6350, 0.0780, 0.1840];
PlotOrbitPlanet(planets.arrival,arrival_date);

mercury = InitialisingPlanets(arrival_date, 1);
mars = InitialisingPlanets(arrival_date, 4);
sun = InitialisingPlanets(arrival_date, 10);

mercury.line_style = '--';
mercury.line_colour = [0.78, 0.78, 0.78];
PlotOrbitPlanet(mercury, arrival_date);

l = legend('1^{st} Interplanetary Arc','2^{nd} Interplanetary Arc', 'Departure Orbit', 'Flyby Orbit', 'Arrival Orbit', 'Intermediate Orbits');
l.Location = 'northeast';

mars.line_style = '--';
mars.line_colour = [0.78, 0.78, 0.78];
mars.legend = false;
PlotOrbitPlanet(mars, arrival_date);

sun.legend = false;
PlotOrbitPlanet(sun,arrival_date);

axis equal;
xlabel('$r_x$ [AU]');
ylabel('$r_y$ [AU]');
zlabel('$r_z$ [AU]');
hold off;
view([1 1 0.4])

figure;

% plot
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');

set(gcf, 'Position',  [0, 0, 1000, 600]);

plot3(X_departure_to_flyby(:,1),X_departure_to_flyby(:,2),X_departure_to_flyby(:,3),'.-','Color', [0, 0.4470, 0.7410],'LineWidth',0.8);

hold on
grid on

planets.departure.line_colour = [0.4660, 0.6740, 0.1880];
planets.departure.line_style = '.-';
PlotOrbitPlanet(planets.departure,flyby_date);

planets.flyby.line_colour = [0.4940, 0.1840, 0.5560];
planets.flyby.line_style = '.-';
PlotOrbitPlanet(planets.flyby,flyby_date);

mercury = InitialisingPlanets(flyby_date, 1);
mars = InitialisingPlanets(flyby_date, 4);
sun = InitialisingPlanets(flyby_date, 10);

planets.arrival.line_style = '.-';
planets.arrival.line_colour = [0.6350, 0.0780, 0.1840];
PlotOrbitPlanet(planets.arrival,flyby_date);

mercury.line_style = '--';
mercury.line_colour = [0.78, 0.78, 0.78];
PlotOrbitPlanet(mercury, flyby_date);

l = legend('1^{st} Interplanetary Arc', 'Departure Orbit', 'Flyby Orbit', 'Arrival Orbit', 'Intermediate Orbits');
l.Location = 'northeast';

mars.line_style = '--';
mars.line_colour = [0.78, 0.78, 0.78];
mars.legend = false;
PlotOrbitPlanet(mars, flyby_date);

sun.legend = false;
PlotOrbitPlanet(sun,flyby_date);

axis equal;
xlabel('$r_x$ [AU]');
ylabel('$r_y$ [AU]');
zlabel('$r_z$ [AU]');
view([1 1 0.4])

figure;

% plot
set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');

set(gcf, 'Position',  [0, 0, 1000, 600]);

hold on
grid on

planets.departure.line_colour = [0.4660, 0.6740, 0.1880];
planets.departure.line_style = '.-';
PlotOrbitPlanet(planets.departure,departure_date);

planets.flyby.line_colour = [0.4940, 0.1840, 0.5560];
planets.flyby.line_style = '.-';
PlotOrbitPlanet(planets.flyby,departure_date);

planets.arrival.line_style = '.-';
planets.arrival.line_colour = [0.6350, 0.0780, 0.1840];
PlotOrbitPlanet(planets.arrival,departure_date);

mercury = InitialisingPlanets(departure_date, 1);
mars = InitialisingPlanets(departure_date, 4);
sun = InitialisingPlanets(departure_date, 10);

mercury.line_style = '--';
mercury.line_colour = [0.78, 0.78, 0.78];
PlotOrbitPlanet(mercury, departure_date);

l = legend('Departure Orbit', 'Flyby Orbit', 'Arrival Orbit', 'Intermediate Orbits');
l.Location = 'northeast';

mars.line_style = '--';
mars.line_colour = [0.78, 0.78, 0.78];
mars.legend = false;
PlotOrbitPlanet(mars, departure_date);

sun.legend = false;
PlotOrbitPlanet(sun,departure_date);

axis equal;
xlabel('$r_x$ [AU]');
ylabel('$r_y$ [AU]');
zlabel('$r_z$ [AU]');
view([1 1 0.4])
end
