function [time_window] = WindowCreation (starting_date, end_date)
% Create a matrix witth all the days or months within two input dates
% t 6-vector     [year, month, day, hour, minute, second]
%                In order to make the function working it's necessear to pass t as a (2,1)
%                vector or (3,1) vector. The function is limited in changing month or day
%                between two given dates, it doesn't take in consideration any change of
%                hours, minutes, seconds.
% 
% INPUT:
% starting_date        first date considered passed as a vector as
%                       specified above
% end_date             last day passed as a vector as specified above
%
% OUTPUT:
% time window           Matrix containing all the dates within the two
%                       specified in input
%
% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

n=length(starting_date); 

switch (n)
    % Required change : months between two dates
    case (2)
        window_month = ChangeOfMonths(starting_date,end_date);
        time_window = zeros(length(window_month),6);
        time_window(:,1) = window_month(:,1);
        time_window(:,2) = window_month(:,2);
        time_window(:,3) = 1;

    % Required change : days between to dates  
    case (3)
        window_day = ChangeOfDays(starting_date,end_date);
        time_window = zeros(length(window_day),6);
        time_window(:,1) = window_day(:,1);
        time_window(:,2) = window_day(:,2);
        time_window(:,3) = window_day(:,3);
    otherwise
        disp("Invalid input format. Should be in the format of [ YYYY MM DD ] or [ YYYY MM ].");
end
        