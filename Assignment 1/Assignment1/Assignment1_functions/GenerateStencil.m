function [output] = GenerateStencil(complete_time_window, index)
% Generates the 3D stencil required for the optimiser. This is used 
% to calculate the adjacent points in the date space in order to calcualte
% which direction, if any, has a smaller cost
%
% PROTOTYPE
%   GenerateStencil (complete_time_window,index)
%
% INPUTS
%   complete_time_window The entire time domain for all of the possible dates to choose from
%               
%   index The central index to compare against
% 
% OUTPUTS
%   output A structure containing information about the adjacent nodes
% VERSIONS
%   28/11/2019  First version



    central_date.departure = complete_time_window(index.departure, :);
    central_date.flyby = complete_time_window(index.flyby, :);
    central_date.arrival = complete_time_window(index.arrival, :);

    central.date = central_date;

    orbit.departure = InitialisingPlanets(central_date.departure, 5);
    orbit.flyby = InitialisingPlanets(central_date.flyby, 3);
    orbit.arrival = InitialisingPlanets(central_date.arrival, 2);

    central.orbit = orbit;
    central.index = index;   

    output.central = central;

    %{
        This block of ifs is to avoid having the stencil collide with the
        boundaries of both of the start of the array i.e. trying going
        below 1 or past the boundary of the complete_time_window i.e. past
        the size actual size of it. Therefore, we offset the index by 1 to
        keep it outside of this range.
    %}
    if index.departure == 1 
        index.departure = index.departure + 1;
    end
    if index.departure == length(complete_time_window)
        index.departure = index.departure - 1;
    end
    if index.flyby == 1 
        index.flyby = index.flyby + 1;
    end
    if index.flyby == length(complete_time_window)
        index.flyby = index.flyby - 1;
    end
    if index.arrival == 1 
        index.arrival = index.arrival + 1;
    end
    if index.arrival == length(complete_time_window)
        index.arrival = index.arrival - 1;
    end      
    
    for i = 1 : 6
        switch i
            case 1
                node_index.departure = index.departure - 1;
                node_index.flyby = index.flyby;
                node_index.arrival = index.arrival;

                date.departure = complete_time_window(index.departure - 1, :);
                date.flyby = complete_time_window(index.flyby, :);
                date.arrival = complete_time_window(index.arrival, :);
            case 2
                node_index.departure = index.departure + 1;
                node_index.flyby = index.flyby;
                node_index.arrival = index.arrival;

                date.departure = complete_time_window(index.departure + 1, :);
                date.flyby = complete_time_window(index.flyby, :);
                date.arrival = complete_time_window(index.arrival, :);
            case 3
                node_index.departure = index.departure;
                node_index.flyby = index.flyby - 1;
                node_index.arrival = index.arrival;

                date.departure = complete_time_window(index.departure, :);
                date.flyby = complete_time_window(index.flyby - 1, :);
                date.arrival = complete_time_window(index.arrival, :);
            case 4
                node_index.departure = index.departure;
                node_index.flyby = index.flyby + 1;
                node_index.arrival = index.arrival;

                date.departure = complete_time_window(index.departure, :);
                date.flyby = complete_time_window(index.flyby + 1, :);
                date.arrival = complete_time_window(index.arrival, :);
            case 5
                node_index.departure = index.departure;
                node_index.flyby = index.flyby;
                node_index.arrival = index.arrival - 1;
                
                date.departure = complete_time_window(index.departure, :);
                date.flyby = complete_time_window(index.flyby, :);
                date.arrival = complete_time_window(index.arrival - 1, :);
            case 6
                node_index.departure = index.departure;
                node_index.flyby = index.flyby;
                node_index.arrival = index.arrival + 1;

                date.departure = complete_time_window(index.departure, :);
                date.flyby = complete_time_window(index.flyby, :);
                date.arrival = complete_time_window(index.arrival + 1, :);
        end    

        orbit.departure = InitialisingPlanets(date.departure, 5);
        orbit.flyby = InitialisingPlanets(date.flyby, 3);
        orbit.arrival = InitialisingPlanets(date.arrival, 2);

        obj(i).index = node_index;
        obj(i).date = date;
        obj(i).orbit = orbit;
    end

    output.nodes = obj;
end