function [bis, bis_year] = LeapYearCheck (t0,tf)

bis_year = []; % bisestile year we have
bis_initial = 0; % if starting year is a leap year and we are passing through febraury
bis_final = 0; % if final year is a leap year and we are passing through febraury
year = t0(1);

while year <= tf(1)
    n = 1;
    % check we have febraury
    if mod(year,400) == 0 && n
        bis_year = [bis_year; year] ;
        if year == t0(1)
            bis_initial = 1;
        elseif year == tf(1)
            bis_final = 1;
        end
        n = 0;
    elseif mod(year,100) == 0 && n
        n = 0;
    elseif mod(year,4) == 0 && n
        n = 0;
        bis_year = [bis_year; year];
        if year == t0(1)
            bis_initial = 1;
        elseif year == tf(1)
            bis_final = 1;
        end
    end
    year = year+1;

end

bis = [bis_initial bis_final];