function [DeltaT] = TimeLawHyp (r_SOI,ecc,a)

%INPUT 
%r_SOI : radius of the sphere of influence of the planet
%ecc : eccentricity of the hyperbola 
%a : semimajor axis of the hyperbola in the absoulte value

%OUTPUT 
%DeltaT = time of flight 

%DESCRIPTION
%The time law starts from the periapsis of the hyperbola, because in this way
%we have set the reference conditions to zero
%We use the position corresponding to the enter in the SOI of the planet as
%the point from where we start to calculate the time on the hyperbola 

% AUTHORS:
%       Bonariol Teodoro
%       Cantoni Alexia
%       Capelli Anita
%       Parisi Adrian

k_p = astroConstants(13);

p = a.*(ecc.^2-1);
theta = acos((1./ecc).*((p./r_SOI)-1)); 

F = 2*atanh(sqrt((ecc-1)./(ecc+1)).*tan(theta./2));
M = ecc.*sinh(F)-F;
DeltaT = sqrt(a.^3./k_p).*M;
