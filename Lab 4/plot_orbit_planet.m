function plot_orbit_planet(planet,time1,time2)

% Plot planet's orbit given an initial date or given an initial and final
% dates
% Input
% Planet
% time1 = initial date
% time2 = final date (if we need to plot just a portion of the orbit
% otherwise entire orbit will be plotted

time_1 = date2mjd2000(time1);

% we find the ephemerides of the planet in the initial date
if planet < 11
    [orbit1,ksun] = uplanet(time_1,planet);
else
    [orbit1] = ephNEO(time_1,planet);
    ksun = astroConstants(4);
end

% if we have just the initial time, we are going to plot an entire
% revolution of the planet
% computing of the semi-major axis in order to find the period of
% revolution
a = orbit1(1);
T = 2*pi*sqrt(a.^3/ksun);
T_days = T/(3600*24);

% initializing the time window
[time_window] = WindowCreation ([2020 1 1],[2099 12 12]);

% setting the days' vector
period_days = [];
for i = 1:size(time_window,1)
    if ismember(time1,time_window(i,:),'rows') == 1
        period_days = time_window((i:(i+round(T_days))),:);
    end
end

% In case we have also the final date
if nargin == 3
    for k = 1:size(period_days,1)
        if ismember(time2,period_days(k,:),'rows') == 1
            period_days = period_days(1:k,:);
            break
        end
    end
end

% computing the position of the planet for every day
r = zeros(3,length(period_days));
for j = 1:size(period_days,1)
    time = date2mjd2000(period_days(j,:));
    if planet < 11
        [orbit1,ksun] = uplanet(time,planet);
    else
        [orbit1] = ephNEO(time,planet);
    end
    [r(:,j)] = kp2rv_orbit(orbit1,ksun);
end  

% plot
AU = astroConstants(2);
r_sun = zeros(3,length(period_days));

set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');

plot3(r(1,:)./AU,r(2,:)./AU,r(3,:)./AU,r(1,1)./AU,r(2,1)./AU,r(3,1)./AU,'or',r_sun(1,:),r_sun(2,:),r_sun(3,:),'*y')
grid on
axis equal
xlabel('rx [AU]')
ylabel('ry [AU]')
zlabel('rz [AU]')
view([1 1 1])
