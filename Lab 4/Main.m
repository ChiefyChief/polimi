%{
    Orbital Mechanics Lab 4  
%}

clear variables;
clc;
close all;

% Add paths to common functions
addpath('../Common');
addpath('../Common/Time');

[window_date_departure] = window_fun ([2020 4 1],[2021 8 1]);
[window_date_arrival] = window_fun ([2022 9 1],[2024 3 1]);

[delta_v, time_of_flight_matrix, ~, ~, ~] = window_deltaV (window_date_departure,window_date_arrival,5,3);
%deltaV is already fine to use in the contour plot

PorkChopPlot(window_date_departure, window_date_arrival, delta_v, time_of_flight_matrix);

plot_orbit_interplanetary([2003 6 2 0 0 0],[2003 12 25 0 0 0],5,3);