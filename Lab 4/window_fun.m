function [window_date] = window_fun (t0,tf)

%t 6-vector: [year, month, day, hour, minute, second]
n=length(t0); 

switch (n)
    % year-month
    case (2)
        window_month = change_month(t0,tf);
        window_date = zeros(length(window_month),6);
        window_date(:,1) = window_month(:,1);
        window_date(:,2) = window_month(:,2);
        window_date(:,3) = 1;

    % year-month-day   
    case (3)
        window_day = change_day(t0,tf);
        window_date = zeros(length(window_day),6);
        window_date(:,1) = window_day(:,1);
        window_date(:,2) = window_day(:,2);
        window_date(:,3) = window_day(:,3);
    otherwise
        disp("I DONT KNOW WHA T TO DO TBHERE ARE NORE THN 2 OR 3 PARAMETESR");
end
        