function [window_date] = change_month (t0,tf)

count_month = tf(2)-t0(2);
count_year = tf(1)-t0(1);
if count_month > 0 
    if count_year == 0
        window_date = zeros(count_month+1,2);
    else
        count_month=count_year*12+count_month;
        window_date = zeros(count_month+1,2);
    end
else
    count_month = 12 - abs(count_month);
    if count_year == 1
        window_date = zeros(count_month+1,2);
    else
        count_month=(count_year-1)*12+count_month;
        window_date = zeros(count_month+1,2);
    end
end
window_date(1,1) = t0(1);
window_date(1,2) = t0(2);
window_date(end,1) = tf(1);
window_date(end,2) = tf(2);
window_date(2:end-1,1)=window_date(1,1);
for i = 2:count_month
    window_date(i,2) = window_date(i-1,2)+1;
    if window_date(i,2) > 12
        window_date(i:end-1,1) = window_date(i-1,1)+1; %change of year
        window_date(i,2) = window_date(i,2)-12;      %shift of the month
    end
end 