function [A, E, THETA, v_1, v_2] = lambert_deltav(departure_orbit, arrival_orbit, time_of_flight, mu)

[r1, v1] = kp2rv_orbit(departure_orbit, mu);
[r2, v2] = kp2rv_orbit(arrival_orbit, mu);

[A,P,E,ERROR,VI,VF,TPAR,THETA] = lambertMR(r1, r2, time_of_flight, mu, 0, 0, 0, 2);

v_1 = abs(norm(VI'-v1));
v_2 = abs(norm(v2-VF'));
