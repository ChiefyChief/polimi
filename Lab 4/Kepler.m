function [t,y] = Kepler(k,T,y0)

% Function to solve the equation of motion of a satellite
% Input
% k = gravitational parameter [km^3/s^2]
% T = time vector (initial and final times) [s]
% y0 = initial conditions vector 6x1
% Output
% t = time vector [s]
% y = position and velocity vector 6x1

tspan=T;
%odefun
odefun = @(t,y) [y(4);
    y(5);
    y(6);
    -k.*y(1)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -k.*y(2)./(y(1).^2+y(2).^2+y(3).^2)^1.5;
    -k.*y(3)./(y(1).^2+y(2).^2+y(3).^2)^1.5];
options = odeset('RelTol',1e-13,'AbsTol',1e-14);

%result
[t,y] = ode113(odefun,tspan,y0,options);