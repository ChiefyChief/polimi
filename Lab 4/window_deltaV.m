function [deltaV, time_of_flight_matrix, a_transfer,e_transfer,theta_transfer] = window_deltaV (window_date_departure,window_date_arrival,planet1,planet2,C3)

%                   1:   Mercury
%                   2:   Venus
%                   3:   Earth
%                   4:   Mars
%                   5:   Jupiter
%                   6:   Saturn
%                   7:   Uranus
%                   8:   Neptune
%                   9:   Pluto
%                   10:  Sun

deltaV = zeros(length(window_date_arrival),length(window_date_departure));
a_transfer = zeros(length(window_date_arrival),length(window_date_departure));
e_transfer = zeros(length(window_date_arrival),length(window_date_departure));
theta_transfer = zeros(length(window_date_arrival),length(window_date_departure));
time_of_flight_matrix = zeros(length(window_date_arrival),length(window_date_departure));

for i = 1:size(window_date_departure,1)
    
    for j = 1:size(window_date_arrival,1)
       
        time_departure = (date2mjd2000(window_date_departure(i,:))); 
        time_arrival = (date2mjd2000(window_date_arrival(j,:)));
        time_of_flight = 24*3600*(time_arrival - time_departure);
        
        time_of_flight_matrix(j,i) = time_of_flight;
        
        if planet1 < 11
            [departure_orbit, mu_sun] = uplanet(time_departure, planet1);
        else
            [departure_orbit,~,~,~] = ephNEO(time_departure,planet1);
        end
        
        if planet2 < 11
            [arrival_orbit, mu_sun] = uplanet(time_arrival, planet2);
        else
            [arrival_orbit,~,~,~] = ephNEO(time_arrival,planet2);
        end

        [A,E,THETA,v_departure,v_arrival] = lambert_deltav(departure_orbit, arrival_orbit, time_of_flight, mu_sun);
        
        if nargin == 5
            if v_departure > C3
                v_departure=nan;
                disp ('v_dep is larger than escape velocity of the launcher')
            end
        end
        
        deltaV(j,i) = v_departure+v_arrival;
        a_transfer(i,j) = A;
        e_transfer(i,j) = E;
        theta_transfer(i,j) = THETA;
        
    end
end