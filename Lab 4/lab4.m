clear
clc
%%Lab4
time=zeros(2,1);

t0_departure= date2mjd2000();
tf_departure= date2mj2000();

t0_arrival=date2mj2000();
tf_arrival=date2mj2000();

d_count = (t0_departure - tf_departure)/86400;
a_count = (t0_arrival - tf_arrival)/86400;

DV = zeros(a_count, d_count);

departure_count = 1;

for departure_time = t0_departure:86400:tf_departure
    
    arrival_count = 1;
    
    for arrival_time = t0_arrival:86400:tf_arrival
        departure = fun(departure_time);
        arrival = fun(arrival_time);
        
        time(1) = 24*3600*(date2mjd2000(departure));
        time(2) = 24*3600*(date2mjd2000(arrival));
   
        [orbit1,ksun] = uplanet(mjd2000,3);
        [orbit2,ksun] = uplanet(mjd2000,4);
        
        [A,E,THETA,deltav] = lambert_deltav(orbit1,orbit2,time,ksun);
        DELTAV(arrival_count,departure_count) = deltav;
        
        arrival_count = arrival_count + 1;
    end
    
    departure_count = departure_count + 1;
end
