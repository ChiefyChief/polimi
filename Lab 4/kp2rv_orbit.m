function [r,v]=kp2rv_orbit(orbit,mu)

% Function to find the r and v vector starting from the orbital parameters
% Input
% orbit = 1x6 vector containing the orbital parameters in this order:
%   a = semimajor axis [km]
%   e = eccentricity [-]
%   i = inclination [rad]
%   OMEGA = right ascension of the ascending node [rad]
%   omega = argument of the perigee [rad]
%   theta = true anomaly [rad]
% mu = gravitational parameter
% Output in the inertial frame
% r = position vector 3x1 [km]
% v = velocity vector 3x1 [km/s]

% Orbital parameters
a = orbit(1);
e = orbit(2);
i = orbit(3);
OMEGA = orbit(4);
omega = orbit(5);
theta = orbit(6);

% Rotation matrices
R1 = [cos(OMEGA)     sin(OMEGA)     0
    -sin(OMEGA)    cos(OMEGA)     0
        0              0          1];

R2 = [1     0       0
    0   cos(i)  sin(i)
    0  -sin(i)  cos(i)];

R3 = [cos(omega)   sin(omega)   0
    -sin(omega)  cos(omega)   0
          0               0         1];

% Compute r_peri and v_peri in the perifocal frame
p = a*(1-e.^2);
h = sqrt(mu*p);
r_peri = (h.^2./mu)*1./(1+e*cos(theta))*[cos(theta);sin(theta);0];
v_peri = mu./h*[-sin(theta);(e+cos(theta));0];

% Compute of r and v
r = (R1'*(R2'*(R3'*r_peri)));
v = (R1'*(R2'*(R3'*v_peri)));
