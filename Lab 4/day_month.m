function [days] = day_month (month)

switch (month)
    case {11,04,06,09}
        days = 30;
    case (02)
        days = 28;
    case {01,03,05,07,08,10,12}
        days = 31;
end
return