function plot_orbit_interplanetary (time_departure, time_arrival, planet_1, planet_2)

% Function to plot an interplanetary trajectory given the departure and
% arrival times and the planets

time_departure_s = date2mjd2000(time_departure);
time_arrival_s = date2mjd2000(time_arrival);
time_of_flight = 24*3600*(time_arrival_s - time_departure_s);

AU = astroConstants(2);

if planet_1 < 11
    [orbit1,ksun] = uplanet(time_departure_s,planet_1);
else
    [orbit1] = ephNEO(time_departure_S,planet_1);
    ksun = astroConstants(4);
end
[R_1] = kp2rv_orbit(orbit1,ksun);

if planet_2 < 11
    [orbit2,ksun] = uplanet(time_arrival_s,planet_2);
else
    [orbit2] = ephNEO(time_arrival_s,planet_2);
    ksun = astroConstants(4);
end
[R_2] = kp2rv_orbit(orbit2,ksun);

[A,P,E,ERROR,VI,VF,TPAR,THETA] = lambertMR(R_1(:,1), R_2(:,end), time_of_flight, ksun, 0, 0, 0, 2);

X0 = [R_1(:,1); VI'];
[~,X_T] = Kepler(ksun,[0 time_of_flight],X0);

set(0,'DefaultTextFontname', 'CMU Serif');
set(0,'DefaultAxesFontName', 'CMU Serif');
set(0,'defaulttextinterpreter','latex');
figure(4);

plot3(R_1(1)/AU,R_1(2)/AU,R_1(3)/AU,'ok')
hold on
grid on
plot3(R_2(1)/AU,R_2(2)/AU,R_2(3)/AU,'og')
plot3(X_T(:,1)/AU,X_T(:,2)/AU,X_T(:,3)/AU,'r','LineWidth',2)
%legend('Initial position','Final position','Transfer orbit','Planet1','Sun','Planet2')
axis equal;
xlabel('rx [AU]')
ylabel('ry [AU]')
zlabel('rz [AU]')
